/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/4/14
 * Time: 9:43 PM
 */

//----------- CvForm Object ------------------------------------------
function CvForm() {
    this.skillInfo = {
        tpl:            '#skill_row_template',
        lastAttr:       'data-skill_group',
        lastAttrVal:    'last',
        wrapper:        '#skill_wrapper'
    };

    this.educationInfo = {
        tpl:            '#education_row_template',
        lastAttr:       'data-education_group',
        lastAttrVal:    'last',
        wrapper:        '#education_wrapper'
    };

    this.work_experienceInfo = {
        tpl:            '#work_experience_row_template',
        lastAttr:       'data-work_experience_group',
        lastAttrVal:    'last',
        wrapper:        '#work_experience_wrapper'
    };

    this.other_infoInfo = {
        tpl:            '#other_info_row_template',
        lastAttr:       'data-other_info_group',
        lastAttrVal:    'last',
        wrapper:        '#other_info_wrapper'
    };

    this.groupTypeNames = [
        'skillInfo',
        'educationInfo',
        'work_experienceInfo',
        'other_infoInfo'
    ];

    this.rowsWithCalendar = {
        education: [
            'education_start_date_val',
            'education_finish_date_val'
        ],
        'work_experience': [
            'work_experience_start_date_val',
            'work_experience_finish_date_val'
        ]
    };
}

/**
 * The method for adding a new input row for Skill, Education, etc.
 *
 * @param rowType
 */
CvForm.prototype.addNewGroup = function(rowType) {
    var _self = this;

    //get a current group parameters fot further work
    var rowParams = this[rowType + 'Info'];

    //current last input row
    var lastRow = $('div[' + rowParams.lastAttr + '="' + rowParams.lastAttrVal + '"]');

    // a new row to be added
    var newRowHtml = $(rowParams.tpl)
                    .clone()
                    .removeAttr('id')
                    .attr(rowParams.lastAttr, rowParams.lastAttrVal);

    if ( lastRow.length > 0) {
        lastRow.after(newRowHtml);
    } else {
        $(rowParams.wrapper).prepend(newRowHtml);
    }

    //set up new event on delete button
    newRowHtml.find('a.delete_' + rowType).on('click', function() {
        _self.deleteGroup($(this), rowType);

        return false;
    });

    newRowHtml.find('textarea')
        .addClass('wysiwyg_area')
        .cleditor();

    this.recalculateRowsOrder(rowType);

    //there is already a "new" last group. Remove this one
    lastRow.removeAttr(rowParams.lastAttr);

    this.addCalendarIfRequired(newRowHtml, rowType);
};

/**
 * Add datepicker calendar if required for this section
 *
 * @param row
 * @param type
 */
CvForm.prototype.addCalendarIfRequired = function(row, type) {
    if ( this.rowsWithCalendar.hasOwnProperty(type) ) {
        for(var i in this.rowsWithCalendar[type]) {
            var el = row.find('.' + this.rowsWithCalendar[type][i]);
            el.removeClass('hasDatepicker');
            el.datepicker({
                "dateFormat":   'yy-mm-dd',
                changeMonth:    true,
                changeYear:     true,
                yearRange:      "-80:+5"
            });
        }
    }
};

CvForm.prototype.recalculateRowsOrder = function(rowType) {
    var rows = $('.' + rowType + '_row').slice(0, -1).each(function(index, row) {
        $(row).attr('data-' + rowType + '-row-order', index);
    });
};

/**
 * Processes the form before sending data.
 *
 * @param event
 */
CvForm.prototype.processSubmitCvData = function(event) {
    //We don't need template fields on a server, so let's remove them before post
    var elementsToBeRemoved = [];

    for(var groupId in this.groupTypeNames) {
        elementsToBeRemoved.push(this[this.groupTypeNames[groupId]].tpl);
    }

    $(elementsToBeRemoved.join(',')).remove();
};

CvForm.prototype.deleteGroup = function(clickedGroup, rowType) {
    var rowParams = this[rowType + 'Info'];

    var rowToDelete = clickedGroup.parents('.' + rowType + '_row');
    var isLast      = rowToDelete.attr(rowParams.lastAttr) === rowParams.lastAttrVal;
    var rowOrder    = rowToDelete.attr('data-' + rowType + '-row-order');

    //if it is the last row then move "last" marker to previous if exists
    if ( isLast ) {
        var previousRow = $('div[data-' + rowType + '-row-order=' + (rowOrder-1) + ']');
        previousRow.attr('data-' + rowType + '_group', 'last');
    }

    rowToDelete.remove();
    this.recalculateRowsOrder(rowType);
};

//----------- CvManager Object ------------------------------------------

function CvManager() {
    this.ajaxPath = 'api/v1/cvData';
}

CvManager.prototype.deleteCv = function(cvId) {
    if ( confirm('Delete selected CV?') ) {
        $.ajax({
            url:    this.ajaxPath + '/' + cvId,
            type:   'DELETE',

            success: function(data, textStatus, jqXHR) {
                if ( data.isSuccess == true ) {
                    window.location.reload();
                } else {
                    alert('Error while deleting CV');
                }

            },

            error:  function(jqXHR, textStatus, errorThrown) {
                alert('Sorry an error while deleting');
            }
        });
    }
};


//--------------------- On Load actions to be done -----------------------------------
$(document).ready(function() {
    var cvForm = new CvForm;
    var cvManager = new CvManager();

    $("textarea.wysiwyg_area").cleditor();

    $('#personal_info_birth_date, .education_start_date_val, ' +
        '.education_finish_date_val, .work_experience_start_date_val, ' +
        '.work_experience_finish_date_val')
        .datepicker({
            "dateFormat":   'yy-mm-dd',
            changeMonth:    true,
            changeYear:     true,
            yearRange:      "-80:+5"
        });

    $('#add_skill').on('click', function() {
        cvForm.addNewGroup('skill');
    });

    $('#add_education').on('click', function() {
        cvForm.addNewGroup('education');
    });

    $('#add_work').on('click', function() {
        cvForm.addNewGroup('work_experience');
    });

    $('#add_other_info').on('click', function() {
        cvForm.addNewGroup('other_info');
    });

    $('#cvDataForm').on('submit', function(event) {
        cvForm.processSubmitCvData(event);
    });

    $('.delete_skill').on('click', function() {
        cvForm.deleteGroup($(this), 'skill');

        return false;
    });

    $('.delete_education').on('click', function() {
        cvForm.deleteGroup($(this), 'education');

        return false;
    });

    $('.delete_work_experience').on('click', function() {
        cvForm.deleteGroup($(this), 'work_experience');

        return false;
    });

    $('.delete_other_info').on('click', function() {
        cvForm.deleteGroup($(this), 'other_info');

        return false;
    });

    $('.cv_delete_button').on('click', function(event) {
        var cvId = $(this).parents('.cv_list_row').children('.cv_id').html();

        cvManager.deleteCv(cvId);

        return false;
    });
});