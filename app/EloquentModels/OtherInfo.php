<?php
/**
 * Project: cv-manager
 * Date: 12.02.2015
 * Time: 23:54
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

class OtherInfo extends Eloquent
{
    protected $table = 'other_info';

    public function personalInfo()
    {
        return $this->belongsTo('PersonalInfo');
    }
}