<?php
/**
 * Project: cv-manager
 * Date: 05.02.2015
 * Time: 10:51
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

class Skill extends Eloquent
{
    protected $table = 'skills';

    public function personalInfo()
    {
        return $this->belongsTo('PersonalInfo');
    }
}