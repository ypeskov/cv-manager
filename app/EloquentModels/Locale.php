<?php
/**
 * Project: cv-manager
 * Date: 03.02.2015
 * Time: 10:41
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

class Locale extends Eloquent
{
    protected $table = 'locales';

    public function personalInfos()
    {
        return $this->hasMany('PersonalInfo');
    }
}