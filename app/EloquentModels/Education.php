<?php
/**
 * Project: cv-manager
 * Date: 05.02.2015
 * Time: 11:04
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

class Education extends Eloquent
{
    protected $table = 'education';

    public function personalInfo()
    {
        return $this->belongsTo('PersonalInfo');
    }
}