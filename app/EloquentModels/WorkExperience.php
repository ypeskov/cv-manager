<?php
/**
 * Project: cv-manager
 * Date: 05.02.2015
 * Time: 11:07
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

class WorkExperience extends Eloquent
{
    protected $table = 'work_experience';

    protected $fillable = [
        'company_name',
        'date_start',
        'date_finish',
        'position',
        'description',
        'personal_info_id',
    ];

    public function personalInfo()
    {
        return $this->belongsTo('PersonalInfo');
    }
}