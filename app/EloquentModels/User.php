<?php

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
     * This property sets validation rules for the model
     *
     * @var array
     */
    protected $validationRules = [
        'email'		=> 'Required|Between:3,100|Email|Unique:users',
        'password'	=> 'Required|AlphaNum|Between:3,12|Confirmed'
    ];

    protected $fillable = [
        'email',
        'password',
    ];

    /**
     * Stores validation errors
     *
     * @var array
     */
    protected $errors = [];

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Validates the password for rules
     *
     * @param array	$data
     *
     * @return bool
     */
    public function validatePassword($data)
    {
        $v = Validator::make($data,
            ['password' => $this->validationRules['password']
        ]);

        if ( $v->fails() ) {
            $this->errors = $v->errors();

            return false;
        }

        return true;
    }

    /**
     * Validates the user for model rules
     *
     * @param array	$user
     *
     * @return bool
     */
    public function validate($user)
    {
        $v = Validator::make($user, $this->validationRules);

        if ( $v->fails() ) {
            $this->errors = $v->errors();

            return false;
        }

        return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function personalInfo()
    {
        return $this->hasMany('PersonalInfo');
    }
}
