<?php
/**
 * Project: cv-manager
 * Date: 05.02.2015
 * Time: 8:19
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

class PersonalInfo extends Eloquent
{
    protected $table = 'personal_info';

    protected $fillable = [
        'cv_name',
        'first_name',
        'last_name',
        'birth_date',
        'email',
        'skype',
        'contact_phone',
        'locale_id',
        'user_id'
    ];

    public function locale()
    {
        return $this->belongsTo('Locale');
    }

    public function skills()
    {
        return $this->hasMany('Skill');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function educations()
    {
        return $this->hasMany('Education');
    }

    public function workExperiences()
    {
        return $this->hasMany('WorkExperience');
    }
}