<?php
/**
 * Project: cv-manager
 * Date: 29.01.2015
 * Time: 22:29
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Cv\Generator\Html;

use CvLib\Cv\Generator\AbstractCvGenerator;

class CvGenerator extends AbstractCvGenerator
{
    public function __construct($outputFormat)
    {
        parent::__construct($outputFormat);
    }

    /**
     * Generates response output for Cv.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function getOutput(array $data)
    {
        return \Response::view($this->getViewOnFormat($this->outputFormat), ['data' => $data]);
    }
}