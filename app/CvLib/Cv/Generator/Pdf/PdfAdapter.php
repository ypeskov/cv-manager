<?php
/**
 * Project: cv-manager
 * Date: 30.01.2015
 * Time: 9:17
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Cv\Generator\Pdf;

use mikehaertl\wkhtmlto\Pdf;

class PdfAdapter
{
    /**
     * Storec instance of PDF generator class
     * @var \TCPDF
     */
    protected $pdf;

    public function __construct()
    {
        $this->pdf = new Pdf();
    }

    /**
     * Adds to pdf object a new page with html content.
     *
     * @param string    $html
     *
     * @return $this
     */
    public function addHtmlContent($html)
    {
        $this->pdf->addPage($html);

        return $this;
    }

    /**
     * @return bool
     */
    public function getPdfOutput()
    {
        return $this->pdf->send();
    }

//    public function __call($method, array $args)
//    {
//        return \call_user_func_array([$this->pdf, $method], $args);
//    }
}