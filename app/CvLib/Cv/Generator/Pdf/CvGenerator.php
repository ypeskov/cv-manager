<?php
/**
 * Project: cv-manager
 * Date: 29.01.2015
 * Time: 22:29
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Cv\Generator\Pdf;

use CvLib\Cv\Generator\AbstractCvGenerator;

class CvGenerator extends AbstractCvGenerator
{
    /**
     * Store the current pdf class.
     *
     * @var PdfAdapter
     */
    protected $pdf;

    public function __construct($outputFormat)
    {
        parent::__construct($outputFormat);

        $this->pdf = new PdfAdapter();
    }

    /**
     * Renders and return http binary response application/pdf
     *
     * @param array $data
     *
     * @return mixed
     */
    public function getOutput(array $data)
    {
        $html =  \View::make($this->getViewOnFormat($this->outputFormat), ['data' => $data])
            ->render();

        return $this->pdf->addHtmlContent($html)->getPdfOutput();
    }
}