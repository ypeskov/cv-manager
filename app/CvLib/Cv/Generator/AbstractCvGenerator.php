<?php
/**
 * Project: cv-manager
 * Date: 29.01.2015
 * Time: 22:46
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Cv\Generator;

use \Lang;

abstract class AbstractCvGenerator implements CvGeneratorInterface
{
    /**
     * Stores the current output format for CV.
     *
     * @var string
     */
    protected $outputFormat;

    public function __construct($outputFormat)
    {
        $this->outputFormat = $outputFormat;
    }

    abstract public function getOutput(array $data);

    /**
     * Return template name depending of format.
     *
     * @param string    $format
     * @return string
     * @throws \Exception
     */
    protected function getViewOnFormat($format)
    {
        $format = strtolower($format);

        if ( ! CvGeneratorFactory::isFormatAvailable($format) ) {
            throw new \Exception(Lang::get('unsupported_format'));
        }

        $tplName = 'cv.cv_' . $format;

        return $tplName;
    }
}