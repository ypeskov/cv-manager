<?php
/**
 * Project: cv-manager
 * Date: 29.01.2015
 * Time: 22:26
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Cv\Generator;

class CvGeneratorFactory
{
    static protected $cvAvailableFormats = [
        'html', 'pdf',
    ];

    /**
     * Check that format is available for output.
     *
     * @param string $format
     * @return bool
     */
    public static function isFormatAvailable($format='')
    {
        return in_array($format, self::$cvAvailableFormats);
    }

    static public function instance($outputFormat=null)
    {
        $className = __NAMESPACE__ . '\\' . ucfirst($outputFormat) . '\\' . 'CvGenerator';

        return new $className($outputFormat);
    }
}