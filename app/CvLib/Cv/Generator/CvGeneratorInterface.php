<?php
/**
 * Project: cv-manager
 * Date: 29.01.2015
 * Time: 22:59
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Cv\Generator;

interface CvGeneratorInterface
{
    public function __construct($outputFormat);
}