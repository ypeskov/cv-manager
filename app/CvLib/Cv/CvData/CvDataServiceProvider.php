<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/5/14
 * Time: 9:09 AM
 */

namespace CvLib\Cv\CvData;

use Illuminate\Support\ServiceProvider;
use CvLib\Repository\CvData\CvDataRepositoryFactory;

class CvDataServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('CvLib\Cv\CvData\CvDataInterface', function($app, $params) {
            $factory = new CvDataRepositoryFactory();

            $cvDataValidator = \App::make('CvLib\Cv\Validators\CvDataValidatorChainInterface');

            return new CvData($factory->getInstance(), $cvDataValidator);
        });
    }
} 