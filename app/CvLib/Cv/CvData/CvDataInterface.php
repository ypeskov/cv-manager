<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/5/14
 * Time: 9:28 AM
 */

namespace CvLib\Cv\CvData;


interface CvDataInterface 
{
    public function checkData();

    public function setData(array $cvData);

    public function save(array $data=null);

    public function update($personalInfoId, array $data);

    public function loadCvData($userId);

    public function getCvInfo($cvId=null);

    public function deleteCvData($cvId);

    public function cvBelongsToUser($personalInfoId, $userId);

    static public function getEmptyCvEntity();
} 