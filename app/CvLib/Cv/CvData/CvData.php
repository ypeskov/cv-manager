<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/5/14
 * Time: 9:04 AM
 */

namespace CvLib\Cv\CvData;

use CvLib\Cv\Validators\CvDataValidatorChain;
use CvLib\Cv\Validators\CvDataValidatorException;
use CvLib\Repository\CvData\CvDataRepositoryInterface;
use CvLib\Cv\Validators\CvDataValidatorChainInterface;

class CvData implements CvDataInterface
{
    protected $cvDataRepository;

    protected $validator;

    protected $data = [];

    /**
     * @param CvDataRepositoryInterface $cvDataRepository
     * @param CvDataValidatorChainInterface $validatorChain
     * @param array $data
     */
    public function __construct(CvDataRepositoryInterface $cvDataRepository,
                                CvDataValidatorChainInterface $validatorChain,
                                array $data=[])
    {
        $this->cvDataRepository = $cvDataRepository;
        $this->data             = $data;

        $this->validator    = $validatorChain;
        $this->validator->setData($data);
    }

    /**
     * Checks if CV data fits into checking rules defined by validators
     *
     * @return bool
     * @throws \CvLib\Cv\Validators\CvDataValidatorException
     */
    public function checkData()
    {
        if ( !$this->validator->validate() ) {
            throw new CvDataValidatorException('CV Data is not valid',
                500,
                null,
                $this->validator->getErrors()
            );
        }

        return true;
    }

    /**
     * @param array $cvData
     * @param bool $validate Whether to make validation in place or no
     *
     * @return \CvLib\Cv\CvData\CvData
     */
    public function setData(array $cvData, $validate=true)
    {
        $this->data = $cvData;

        //Update validator data
        $this->validator->setData($this->data);

        if ( $validate ) {
            $this->checkData();
        }

        return $this;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function save(array $data=null)
    {
        if ( $data !== null ) {
            $this->data = $data;
            $this->validator->setData($this->data);
        }

        $this->checkData();

        $this->data = $this->cvDataRepository
            ->saveCvData($this->data)
            ->getCvData();

        return $this;
    }

    public function toArray()
    {
        return $this->data;
    }

    public function update($personalInfoId, array $data=null)
    {
        if ( $data !== null ) {
            $this->data = $data;
            $this->validator->setData($data);
        }

        $this->checkData();

        $this->cvDataRepository->updateCvData($personalInfoId, $this->data);

        return $this;
    }

    /**
     * @param int $userId
     *
     * @throws \PDOException
     */
    public function loadCvData($userId)
    {
        $result = $this->cvDataRepository->loadCvData($userId);

        return $result;
    }

    protected function prepareDates($educations)
    {
        $dateKeys = ['date_start', 'date_finish', 'birth_date', ];

        array_walk_recursive($educations, function(&$val, $key) use ($dateKeys) {
            if ( in_array($key, $dateKeys) ) {
                if ( $val === '0000-00-00' ) {
                    $val = '';
                }
            }
        });

        return $educations;
    }

    /**
     * Returns information about concrete CV.
     *
     * @param int $cvId
     *
     * @return array
     */
    public function getCvInfo($cvId=null)
    {
        $cvId = (int) $cvId;

        $dateProcess = function($data) {
            return $this->prepareDates($data);
        };

        if ( $cvId === null or !is_numeric($cvId) or $cvId === 0 ) {
            throw new \InvalidArgumentException('cvId is invalid');
        }

        $personalInfo   = $dateProcess($this->cvDataRepository->getPersonalInfo($cvId));
        $skills         = $this->cvDataRepository->getSkills($cvId);
        $educations     = $dateProcess($this->cvDataRepository->getEducation($cvId));
        $workExperience = $dateProcess($this->cvDataRepository->getWorkExperience($cvId));

        $otherInfo      = $this->cvDataRepository->getOtherInfo($cvId);

        return [
            'personal_info'     => $personalInfo,
            'skills'            => $skills,
            'educations'        => $educations,
            'work_experiences'  => $workExperience,
            'other_info'        => $otherInfo,
        ];
    }

    /**
     * Delete a record from storage.
     *
     * @param $cvId
     *
     * @return mixed
     */
    public function deleteCvData($cvId)
    {
        return $this->cvDataRepository->deleteCvData($cvId);
    }

    public function cvBelongsToUser($personalInfoId, $userId)
    {
        return $this->cvDataRepository->cvBelongsTouser($personalInfoId, $userId);
    }

    /**
     * Returns an empty array with minimum fields for CV Data.
     *
     * @return array
     */
    static public function getEmptyCvEntity()
    {
        return [
            'personal_info' => [
                'id'            => null,
                'cv_name'       => null,
                'first_name'    => null,
                'last_name'     => null,
                'birth_date'    => null,
                'email'         => null,
                'skype'         => null,
                'contact_phone' =>null,
                'locale_id'     => null,

            ],
            'skills' => [[
                'id'                => null,
                'group_name'        => null,
                'group_content'     => null,
                'personal_info_id'  =>null,
            ]],

            'education' => [[
                'id'                => null,
                'educational_inst'  => null,
                'date_start'        => null,
                'date_finish'       => null,
                'specialty'         => null,
                'personal_info_id'  => null,
            ]],

            'workExperience' => [[
                'id'            => null,
                'company_name'  => null,
                'date_start'    => null,
                'date_finish'   => null,
                'position'      => null,
                'description'   => null,
            ]],

            'other_info'    => [[
                'id'                => null,
                'info_name'         => null,
                'info_value'        => null,
                'personal_info_id'  =>null,
            ]],
        ];
    }
} 