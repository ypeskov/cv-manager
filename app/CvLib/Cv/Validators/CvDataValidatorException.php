<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/5/14
 * Time: 10:02 AM
 */

namespace CvLib\Cv\Validators;


class CvDataValidatorException extends \InvalidArgumentException
{
    protected $errors = [];

    public function __construct($msg, $code, $previousException=null, $errors=[])
    {
        parent::__construct($msg, $code, $previousException);

        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }
} 