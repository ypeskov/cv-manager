<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/1/14
 * Time: 6:09 PM
 */

namespace CvLib\Cv\Validators;


class CvDataValidatorChain implements CvDataValidatorChainInterface
{
    protected $data;

    /**
     * @var array
     */
    protected $rules = [];

    protected $errors = [];

    protected $concreteValidators = [];

    const LOCALE_PARAM_NAME         = 'locale';
    const PERSONALINFO_PARAM_NAME   = 'personal_info';

    /**
     * Creates an instance of chain of validators to run.
     *
     * @param array $data  Data to be validated.
     */
    public function __construct(Array $data)
    {
        $this->data     = $data;

        $this->setConcreteValidators();
    }

    /**
     * Sets new value for cv data
     *
     * @param array $data
     * @return void
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Builds an array of validation name and class which is responsible for it.
     */
    protected function setConcreteValidators()
    {
        $this->concreteValidators = [
            'fieldsAvailability' => 'FieldsAvailabilityValidator',

        ];
    }

    /**
     * Returns an array of validation rules which should be applied to data.
     *
     * @return array
     * @throws \Exception
     */
    protected function getChainOfValidators()
    {
        $validators = [];

        if ( $this->rules ) {
            foreach ($this->rules as $rule) {
                if ( isset($this->concreteValidators[$rule]) ) {
                    $validators[] = $this->concreteValidators[$rule];
                } else {
                    throw new \Exception('Validation rule [' . $rule . '] is not found', 500);
                }
            }
        }

        if ( $this->rules === null ) {
            $validators = $this->concreteValidators;
        }

        return $validators;
    }


    /**
     * Runs validation according to set rules.
     *
     * @param array $rules
     * @return bool
     */
    public function validate(Array $rules=null)
    {
        $this->rules    = $rules;

        $chainIterator = new \ArrayIterator($this->getChainOfValidators());
        $isValid = true;
        foreach($chainIterator as $validator) {
            $validatorClassName = __NAMESPACE__ . '\\' . $validator;
            $result = (new $validatorClassName($this->data, null))->validate();

            if ( $result['isValid'] === false) {
                $isValid = false;

                $this->errors = array_merge($this->errors, $result['errors']);
            }
        }

        return $isValid;
    }

    /**
     * @return array
     */
    public function getErrors() { return $this->errors; }
} 