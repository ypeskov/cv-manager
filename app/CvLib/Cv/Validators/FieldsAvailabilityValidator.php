<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/2/14
 * Time: 10:23 AM
 */

namespace CvLib\Cv\Validators;


class FieldsAvailabilityValidator  extends CvDataAbstractValidator
{
    const LOCALE_PARAM_NAME         = 'locale_id';
    const PERSONALINFO_PARAM_NAME   = 'personal_info';
    const SKILLS_PARAM_NAME         = 'skills';
    const EDUCATION_PARAM_NAME      = 'educations';

    /**
     * Iterate Data and check that all required fields are present.
     *
     * @return Array
     */
    public function validate()
    {
        $ret = true;

        if ( !isset($this->data[self::PERSONALINFO_PARAM_NAME][self::LOCALE_PARAM_NAME]) ) {
            $ret = false;
            $this->errors[] = \Lang::get('app.locale_param_required');
        }

        if ( !isset($this->data[self::PERSONALINFO_PARAM_NAME]) ) {
            $ret = false;
            $this->errors[] = \Lang::get('app.personal_info_param_required');
        }

        if ( !isset($this->data[self::SKILLS_PARAM_NAME]) ) {
            $ret = false;
            $this->errors[] = \Lang::get('app.skills_param_required');
        }

        if ( !isset($this->data[self::EDUCATION_PARAM_NAME]) ) {
            $ret = false;
            $this->errors[] = \Lang::get('app.education_param_required');
        }

        if ( $this->nextValidator instanceof CvDataAbstractValidator ) {
            return $this->nextValidator->validate();
        }

        return ['isValid' => $ret, 'errors' => $this->errors];
    }
} 