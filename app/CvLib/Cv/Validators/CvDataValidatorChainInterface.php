<?php
/**
 * Project: cv-manager
 * Date: 27.01.2015
 * Time: 8:05
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Cv\Validators;

Interface CvDataValidatorChainInterface
{
    public function validate(Array $rules=null);

    public function setData(array $data);

    public function getErrors();
}