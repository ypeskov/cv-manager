<?php
/**
 * Project: cv-manager
 * Date: 27.01.2015
 * Time: 8:08
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Cv\Validators;

use Illuminate\Support\ServiceProvider;
use CvLib\Cv\Validators\CvDataValidatorChain;

class CvDataValidatorChainServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('CvLib\Cv\Validators\CvDataValidatorChainInterface', function($app, $params) {
            return new CvDataValidatorChain([]);
        });
    }
}
