<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/1/14
 * Time: 6:53 PM
 */

namespace CvLib\Cv\Validators;


abstract class CvDataAbstractValidator
{
    protected $data;
    protected $nextValidator;

    protected $errors;

    public function __construct($data, $nextValidator=null)
    {
        $this->data         = $data;
        $this->nextValidator= $nextValidator;
    }

    /**
     *
     * @return Array
     * @throws Exception
     */
    abstract public function validate();
}