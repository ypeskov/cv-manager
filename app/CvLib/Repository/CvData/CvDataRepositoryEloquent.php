<?php
/**
 * Project: cv-manager
 * Date: 05.02.2015
 * Time: 7:57
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Repository\CvData;

use \Skill;
use \DB;
use \PersonalInfo;
use \Education;
use \WorkExperience;
use \OtherInfo;

class CvDataRepositoryEloquent implements CvDataRepositoryInterface
{
    protected $cvData = [];

    public function getCvData()
    {
        return $this->cvData;
    }

    /**
     * Saves the new CV to DB.
     *
     * @param array $cvData
     *
     * @return $this
     */
    public function saveCvData(array $cvData)
    {
        $this->cvData = $cvData;

        DB::beginTransaction();

        $skills         = ( isset($cvData['skills']) ) ? $cvData['skills'] : [];
        $education      = ( isset($cvData['educations']) ) ? $cvData['educations'] : [];
        $workExperience = ( isset($cvData['workExperiences']) ) ? $cvData['workExperiences'] : [];
        $otherInfo      = ( isset($cvData['other_info']) ) ? $cvData['other_info'] : [];

        $personalInfoId = $this->savePersonalInfo($cvData['personal_info']);

        $this->saveSkills($personalInfoId, $skills)
            ->saveEducation($personalInfoId, $education)
            ->saveWorkExperience($personalInfoId, $workExperience)
            ->saveOtherInfo($personalInfoId, $otherInfo);

        DB::commit();

        return $this;
    }

    /**
     * Saves the personal information to DB.
     *
     * @param array $personalInfo
     *
     * @return int
     * @throws \Exception
     */
    protected function savePersonalInfo(array $personalInfo)
    {
        $pi = new PersonalInfo($personalInfo);
        $pi->save();

        if ( $pi instanceof PersonalInfo ) {
            $this->cvData = $pi->toArray();

            return (int) $pi->id;
        } else {
            throw \Exception(Lang::get('app.error_saving_personal_info'));
        }
    }

    /**
     * Store work experience to DB.
     *
     * @param int  $personalInfoId
     * @param array $workExperience
     *
     *
     *@return $this
     */
    protected function saveWorkExperience($personalInfoId, array $workExperience)
    {
        foreach($workExperience as $workItem) {
            //convert to DB field names
            $tmpItem = [
                'company_name'  => $workItem['company_name'],
                'date_start'    => $workItem['date_start'],
                'date_finish'   => $workItem['date_finish'],
                'position'      => $workItem['position'],
                'description'   => $workItem['description'],
                'personal_info_id'  => (int) $personalInfoId,
            ];

            $work = new WorkExperience($tmpItem);
            $work->save();
        }

        return $this;
    }

    /**
     * Store education information to DB.
     *
     * @param int   $personalInfoId
     * @param array $education
     *
     * @return $this
     */
    protected function saveEducation($personalInfoId, array $education)
    {
        $length = count($education);

        $data = [];

        for($i=0; $i < $length; $i++) {
            $data[$i]['educational_inst']   = $education[$i]['educational_inst'];
            $data[$i]['date_start']         = $education[$i]['date_start'];
            $data[$i]['date_finish']        = $education[$i]['date_finish'];
            $data[$i]['specialty']          = $education[$i]['specialty'];
            $data[$i]['personal_info_id']   = (int) $personalInfoId;
        }

        Education::insert($data);

        return $this;
    }

    /**
     * @param int   $personalInfoId
     * @param array $otherInfo
     *
     * @return $this
     */
    protected function saveOtherInfo($personalInfoId, array $otherInfo)
    {
        if ( array_key_exists('info_name', $otherInfo) ) {
            $length = count($otherInfo['info_name']);

            $data = [];

            for($i=0; $i < $length; $i++) {
                $data[$i]['info_name']      = $otherInfo['info_name'][$i];
                $data[$i]['info_value']     = $otherInfo['info_value'][$i];
                $data[$i]['personal_info_id'] = (int) $personalInfoId;
                $data[$i]['updated_at']     = date('Y-m-d H:i:s');
            }

            OtherInfo::insert($data);
        }

        return $this;
    }

    /**
     * Saves skills to DB.
     *
     * @param int   $personalInfoId the value of the ID in DB of personal info which is linked to the these skills
     * @param array $skills         an array of skills
     *
     * @return $this
     */
    public function saveSkills($personalInfoId, array $skills)
    {
        $length = count($skills);

        $data = [];

        for($i=0; $i < $length; $i++) {
            $data[$i]['group_name']   = $skills[$i]['group_name'];
            $data[$i]['group_content']= $skills[$i]['group_content'];
            $data[$i]['personal_info_id'] = (int) $personalInfoId;
        }

        Skill::insert($data);

        return $this;
    }

    /**
     * Updates CV in a storage using DB transaction.
     *
     * @param int   $personalInfoId
     * @param array $cvData
     *
     * @return $this
     */
    public function updateCvData($personalInfoId, array $cvData)
    {
        $this->cvData = $cvData;

        $localeId = (int) $cvData['personal_info']['locale_id'];

        DB::beginTransaction();

        $skills         = ( isset($cvData['skills']) ) ? $cvData['skills'] : [];
        $education      = ( isset($cvData['educations']) ) ? $cvData['educations'] : [];
        $workExperience = ( isset($cvData['workExperiences']) ) ? $cvData['workExperiences'] : [];
        $otherInfo      = ( isset($cvData['other_info']) ) ? $cvData['other_info'] : [];

        $this->updatePersonalInfo($personalInfoId, $localeId, $cvData['personal_info'])
            ->updateSkills($personalInfoId, $skills)
            ->updateEducation($personalInfoId, $education)
            ->updateWorkExperience($personalInfoId, $workExperience)
            ->updateOtherInfo($personalInfoId, $otherInfo);

        DB::commit();

        return $this;
    }

    /**
     * Updates personal_info in storage.
     *
     * @param int   $personalInfoId
     * @param int   $localeId
     * @param array $personalInfo
     *
     * @return $this
     */
    protected function updatePersonalInfo($personalInfoId, $localeId, array $personalInfo)
    {
        $personalInfo['locale_id'] = (int) $localeId;

        DB::table(CvDataRepositoryInterface::TABLE_PERSONAL_INFO)
            ->where('id', '=', (int) $personalInfoId)
            ->update($personalInfo);

        return $this;
    }

    /**
     * Deletes the old skills and then saves new which are related to $personalInfoId
     *
     * @param int   $personalInfoId
     * @param array $skills
     *
     * @return $this
     */
    protected function updateSkills($personalInfoId, array $skills)
    {
        DB::table(CvDataRepositoryInterface::TABLE_SKILLS)
            ->where('personal_info_id', '=', (int) $personalInfoId)
            ->delete();

        $this->saveSkills($personalInfoId, $skills);

        return $this;
    }

    /**
     * Update other info records byt removing old and inserting new.
     *
     * @param int   $personalInfoId
     * @param array $otherInfo
     *
     * @return $this
     */
    protected function updateOtherInfo($personalInfoId, array $otherInfo)
    {
        DB::table(CvDataRepositoryInterface::TABLE_OTHER_INFO)
            ->where('personal_info_id', '=', (int) $personalInfoId)
            ->delete();

        $this->saveOtherInfo($personalInfoId, $otherInfo);

        return $this;
    }

    /**
     * First deletes and after saves new values for education related to definite personal info id.
     *
     * @param int   $personalInfoId
     * @param array $education
     *
     * @return $this
     */
    protected function updateEducation($personalInfoId, array $education)
    {
        DB::table(CvDataRepositoryInterface::TABLE_EDUCATION)
            ->where('personal_info_id', '=', (int) $personalInfoId)
            ->delete();

        $this->saveEducation($personalInfoId, $education);

        return $this;
    }

    protected function updateWorkExperience($personalInfoId, array $workExperience)
    {
        DB::table(CvDataRepositoryInterface::TABLE_WORK_EXPERIENCE)
            ->where('personal_info_id', '=', (int) $personalInfoId)
            ->delete();

        $this->saveWorkExperience($personalInfoId, $workExperience);

        return $this;
    }

    /**
     * Returns the list of personal_data (aka cv).
     *
     * @param int $userId
     *
     * @throws \PDOException
     */
    public function loadCvData($userId)
    {
        $pi = CvDataRepositoryInterface::TABLE_PERSONAL_INFO;
        $l  = CvDataRepositoryInterface::TABLE_LOCALES;

        $cvList = \DB::table($pi)
            ->join($l, $pi . '.locale_id', '=', $l . '.id')
            ->select($pi.'.id AS id',
                    $pi.'.cv_name AS cv_name',
                    $pi.'.first_name as first_name',
                    $pi.'.last_name as last_name',
                    $pi.'.email as email',
                    $pi.'.skype as skype',
                    $pi.'.contact_phone as contact_phone',
                    $l.'.language_long_name as language_long_name')
            ->where($pi.'.user_id', '=', (int) $userId)
            ->get();

        return $cvList;
    }

    /**
     * Returns personal Info record from DB.
     *
     * @param int   $cvId
     *
     * @return Array
     * @throws \PDOException
     */
    public function getPersonalInfo($cvId)
    {
        return (new \PersonalInfo())->find((int) $cvId)->toArray();
    }

    /**
     * Returns an array of skills related to definite Personal Info.
     *
     * @param int   $cvId
     *
     * @return Array
     */
    public function getSkills($cvId)
    {
        $s = CvDataRepositoryInterface::TABLE_SKILLS;

        return Skill::where($s.'.personal_info_id', '=', (int) $cvId)
                    ->get()
                    ->toArray();
    }

    /**
     * @param int       $personalInfoId
     *
     * @return array
     */
    public function getOtherInfo($personalInfoId)
    {
        $oiTable = CvDataRepositoryInterface::TABLE_OTHER_INFO;

        return OtherInfo::where($oiTable.'.personal_info_id', '=', (int) $personalInfoId)
            ->get()
            ->toArray();
    }

    /**
     * Returns an array of education related to definite Personal Info (cvData)
     *
     * @param int   $cvId
     *
     * @return mixed
     */
    public function getEducation($cvId)
    {
        $e = CvDataRepositoryInterface::TABLE_EDUCATION;

        return \Education::where($e.'.personal_info_id', '=', (int) $cvId)
                        ->get()
                        ->toArray();
    }

    /**
     * Returns an array of work experience for definite Personal Info (aka CV or CV Data)
     *
     * @param $cvId
     *
     * @return array
     */
    public function getWorkExperience($cvId)
    {
        $we = CvDataRepositoryInterface::TABLE_WORK_EXPERIENCE;

        return \WorkExperience::where($we.'.personal_info_id', '=', (int) $cvId)
            ->get()
            ->toArray();
    }

    public function deleteCvData($personalInfoId)
    {
        $personalInfoId = (int) $personalInfoId;

        $qtyDeleted = PersonalInfo::destroy($personalInfoId);

        if ( $qtyDeleted > 0 ) {
            $ret    = true;
        } else {
            $ret    = false;
        }

        return $ret;
    }

    /**
     * Verifies whether CV belongs to the user.
     * @param int   $personalInfoId
     * @param int   $userId
     *
     * @return bool
     */
    public function cvBelongsToUser($personalInfoId, $userId)
    {
        return (PersonalInfo::where('id', '=', (int) $personalInfoId)
            ->where('user_id', '=', (int) $userId)
            ->get()
            ->count()) > 0;
    }
}