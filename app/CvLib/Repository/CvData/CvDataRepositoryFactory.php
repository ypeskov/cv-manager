<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/1/14
 * Time: 10:35 AM
 */

namespace CvLib\Repository\CvData;

use CvLib\Repository\AbstractRepositoryFactory;

/**
 * Factory which instantiates a concrete Repository class depending on config cvData.php: engine
 *
 * Class CvDataRepositoryFactory
 * @package CvLib\Repository\CvData
 */
class CvDataRepositoryFactory extends AbstractRepositoryFactory
{
    /**
     * As this class will be initialized by parent abstract method let's define the current namespace
     * for finding this class.
     *
     * @var string
     */
    protected $nameSpace = __NAMESPACE__;

    /**
     * Maps supported DB engines and appropriate concrete repository class.
     *
     * @return void
     */
    protected function setEnginesMap()
    {
        $this->repositoriesEngineMap = [
            self::ENGINE_MYSQL      => 'CvDataRepositoryMysql',
            self::ENGINE_MONGODB    => 'CvDataRepositoryMongodb',
            self::ENGINE_ELOQUENT   => 'CvDataRepositoryEloquent',
        ];
    }
}