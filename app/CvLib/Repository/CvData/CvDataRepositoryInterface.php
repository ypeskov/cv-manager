<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/1/14
 * Time: 10:15 AM
 */

namespace CvLib\Repository\CvData;


interface CvDataRepositoryInterface
{
    const TABLE_PERSONAL_INFO   = 'personal_info';
    const TABLE_SKILLS          = 'skills';
    const TABLE_LOCALES         = 'locales';
    const TABLE_EDUCATION       = 'education';
    const TABLE_WORK_EXPERIENCE = 'work_experience';
    const TABLE_OTHER_INFO      = 'other_info';

    /**
     * @return array
     */
    public function getCvData();

    public function saveCvData(array $cvData);

    public function updateCvData($personalInfoId, array $cvData);

    public function loadCvData($userId);

    public function getPersonalInfo($cvId);

    public function getSkills($cvId);

    public function getEducation($cvId);

    public function getWorkExperience($cvId);

    public function deleteCvData($cvId);

    public function cvBelongsToUser($personalInfoId, $userId);
} 