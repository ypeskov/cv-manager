<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/1/14
 * Time: 10:17 AM
 */

namespace CvLib\Repository\CvData;

use \CvLib\Repository\Mysql\DbConnector;

class CvDataRepositoryMysql implements CvDataRepositoryInterface
{


    /**
     * @var PDO handler to DB
     */
    protected $pdo;

    protected $cvData = [];

    public function __construct()
    {
        $this->pdo = DbConnector::instance()->getPDO();
    }

    /**
     * Saves the new CV to DB.
     *
     * @param array $data
     * @param int $personalInfoId
     *
     * @return mixed
     *
     * @throws \Exception
     * @throws \PDOException
     */
    public function saveCvData(array $data, $personalInfoId=null)
    {
        $this->cvData = $data;

        if ( $this->pdo->beginTransaction() ) {
            $personaInfoId = $this->savePersonalInfo($this->cvData['personal_info']);

            if ( !$this->isInsertedIdValid($personaInfoId) ) {
                throw new \InvalidArgumentException('Personal info ID is required to be int');
            }

            $this->saveEducation($this->cvData['education'], $personaInfoId)
                ->saveSkills($this->cvData['skills'], $personaInfoId)
                ->saveWorkExperience($this->cvData['work'], $personaInfoId);

            $this->pdo->commit();
        } else {
            throw new \Exception('Cannot start transaction');
        }

        return $this;
    }

    /**
     * Saves the personal information to DB.
     *
     * @param array $personalInfo
     *
     * @return int | null
     */
    protected function savePersonalInfo(array $personalInfo)
    {
        $localeId = $this->cvData['locale'];

        $queryStr = "INSERT INTO `" . self::TABLE_PERSONAL_INFO . '` '
            . '(cv_name, first_name, last_name, birth_date, email, skype, contact_phone, locale_id, user_id) '
            . 'VALUES(:cv_name, :first_name, :last_name, :birth_date, :email, :skype, :contact_phone, :locale_id, :user_id)';

        $stmt = $this->pdo->prepare($queryStr);

        $paramsPDO = array_merge($personalInfo, ['locale_id' => $localeId, ]);

        $inserted = $stmt->execute($paramsPDO);

        if ( $inserted ) {
            return (int) $this->pdo->lastInsertId();
        } else {
            return null;
        }
    }

    /**
     * Checks if $insertedId is not null and is integer.
     *
     * @param int $insertedId
     * @return bool
     */
    protected function isInsertedIdValid($insertedId)
    {
        return ( $insertedId === null or !is_int($insertedId) ) ? false : true;
    }

    /**
     * Store work experience to DB.
     *
     * @param array $workExperience
     * @param int  $personalInfoId
     *
     * @return $this
     */
    protected function saveWorkExperience(array $workExperience, $personalInfoId=null)
    {
        $fieldNames = ['company_name', 'date_start', 'date_finish', 'position', 'description', 'personal_info_id',];

        list($preparedSQL, $aggregatedValues) =
            $this->prepareInsertValuesAndSql(self::TABLE_WORK_EXPERIENCE, 'company_name',
                                            $workExperience, $fieldNames, $personalInfoId);

        $this->runPreparedSql($preparedSQL, $aggregatedValues);

        return $this;
    }

    /**
     * Saves skills to DB.
     *
     * @param array $skills an array of skills
     * @param int $personalInfoId the value of the ID in DB of personal info which is linked to the these skills
     *
     * @return $this
     *
     * @throws \Exception
     * @throws \PDOException
     */
    protected function saveSkills(array $skills, $personalInfoId=null)
    {
        $fieldNames = ['group_name', 'group_content', 'personal_info_id', ];

        list($preparedSql, $values)
            = $this->prepareInsertValuesAndSql(self::TABLE_SKILLS, 'group_content', $skills, $fieldNames, $personalInfoId);

        $this->runPreparedSql($preparedSql, $values);

        return $this;
    }

    /**
     * Store education information to DB
     *
     * @param array $education
     * @param int $personalInfoId
     *
     *@return $this
     *
     * @throws \Exception
     * @throws \PDOException
     */
    protected function saveEducation(array $education, $personalInfoId=null)
    {
        $names = ['educational_inst', 'date_start', 'date_finish', 'specialty', 'personal_info_id',];

        list($preparedSQL, $aggregatedValues) =
            $this->prepareInsertValuesAndSql(self::TABLE_EDUCATION, 'educational_inst', $education, $names, $personalInfoId);

        $this->runPreparedSql($preparedSQL, $aggregatedValues);

        return $this;
    }

    /**
     * Return string of prepared SQL string for PDO and values to be bind to this string
     *
     * @param String    $tableName      Name of the table for SQL
     * @param String    $firstFieldName Name of parameter which will be used for counting of total qty
     * @param array     $data           Array of data to be used in SQL to insert
     * @param array     $names          array of field names
     * @param Int       $personalInfoId related to this data personal info ID
     *
     * @return array
     */
    protected function prepareInsertValuesAndSql($tableName, $firstFieldName,
                                                 array $data, array $names, $personalInfoId=null)
    {
        $placeHolders = [];
        $values = [];

        $qtyOfRecords = sizeof($data[$firstFieldName]);

        $sql = 'INSERT INTO `' . $tableName . '` ';
        $sql .= '(' .  join(', ', $names) . ') VALUES';

        for($i=0; $i < $qtyOfRecords; $i++) {
            $data['locale_id'][$i] = 1;

            foreach($names as $name) {
                $placeHolders[$i][] = ':' .  $name . $i;

                if ( $name !== 'personal_info_id' ) {
                    $values[$i][':' . $name . $i] = $data[$name][$i];
                } else {
                    $values[$i][':' . $name . $i] = (int) $personalInfoId;
                }
            }

            $placeholdersRows[] = '(' . join(', ', $placeHolders[$i]) . ')';
        }

        $aggregatedValues = [];
        foreach(array_values($values) as $subArray) {
            $aggregatedValues = array_merge($aggregatedValues, $subArray);
        }

        $sql = $sql . join(', ', $placeholdersRows) . ';';

        return [$sql, $aggregatedValues];
    }

    /**
     * Runs prepared SQL with bound variables
     *
     * @param $preparedSql
     * @param $values
     * @return bool
     */
    protected function runPreparedSql($preparedSql, $values)
    {
        $stmt = $this->pdo->prepare($preparedSql);
        $stmt->execute($values);

        return true;
    }

    /**
     * @param int   $personalInfoId
     * @param array $data
     *
     * @return $this
     * @throws CvDataRepositoryException
     * @throws \Exception
     */
    public function updateCvData($personalInfoId, array $data)
    {
        $this->cvData = $data;

        $localeId = (int) $data['locale'];

        if ( $this->pdo->beginTransaction() ) {
            $this->updatePersonalInfo($personalInfoId, $localeId, $data['personal_info'])
                ->updateSkills($personalInfoId, $data['skills'])
                ->updateEducation($personalInfoId, $data['education'])
                ->updateWorkExperience($personalInfoId, $data['work']);

            $this->pdo->commit();
        } else {
            throw new CvDataRepositoryException('Cannot start update transaction.',
                                                CvDataRepositoryException::TRANSACTION_NOT_STARTED);
        }

        return $this;
    }

    /**
     * Deletes and after stores new values for work experience related to CV id.
     *
     * @param int               $personalInfoId
     * @param array             $workExperience
     *
     * @return $this
     */
    protected function updateWorkExperience($personalInfoId, $workExperience)
    {
        $this->deleteRecords($personalInfoId, self::TABLE_WORK_EXPERIENCE)
            ->saveWorkExperience($workExperience, $personalInfoId);

        return $this;
    }

    /**
     * First deletes and after saves new values for education related to definite personal info id.
     *
     * @param int               $personalInfoId
     * @param array             $education
     *
     * @return $this
     */
    protected function updateEducation($personalInfoId, $education)
    {
        $this->deleteRecords($personalInfoId, self::TABLE_EDUCATION)
            ->saveEducation($education, $personalInfoId);

        return $this;
    }

    /**
     * Deletes the old skills and then saves new which are related to $personalInfoId
     *
     * @param int   $personalInfoId
     * @param array $skills
     *
     * @return $this
     */
    protected function updateSkills($personalInfoId, array $skills)
    {
        $this->deleteRecords($personalInfoId, self::TABLE_SKILLS)
            ->saveSkills($skills, $personalInfoId);

        return $this;
    }

    /**
     * Updates personal information for the CV.
     *
     * @param $personalInfoId
     * @param $localeId
     * @param $personalInfo
     *
     * @return $this
     * @throws CvDataRepositoryException
     * @throws \Exception
     */
    protected function updatePersonalInfo($personalInfoId, $localeId, array $personalInfo)
    {
        $preparedSql = 'UPDATE `' . self::TABLE_PERSONAL_INFO . '` '
            . 'SET cv_name = :cv_name, '
            . 'first_name = :first_name, '
            . 'last_name = :last_name, '
            . 'birth_date = :birth_date, '
            . 'email = :email, '
            . 'skype = :skype, '
            . 'contact_phone = :contact_phone, '
            . 'locale_id = :locale_id, '
            . 'user_id = :user_id '
            . 'WHERE id = :id LIMIT 1';

        $paramsPDO = array_merge($personalInfo, ['id' => (int) $personalInfoId, 'locale_id' => $localeId]);

        $stmt = $this->pdo->prepare($preparedSql);

        try {
            $stmt->execute($paramsPDO);

            if ( $stmt->rowCount() === 0 ) {
                throw new CvDataRepositoryException('CV with id [' . $personalInfoId . '] not updated',
                    CvDataRepositoryException::NOT_UPDATED);
            }
        } catch(CvDataRepositoryException $e) {
            if ( $e->getCode() !== CvDataRepositoryException::NOT_UPDATED ) {
                throw $e;
            }
        } catch(\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Deletes records related to definite personal info ID.
     *
     * @param int       $personalInfoId
     * @param string    $tableName
     *
     * @return $this
     */
    protected function deleteRecords($personalInfoId, $tableName)
    {
        $preparedSql = 'DELETE FROM `' . $tableName . '` WHERE personal_info_id = :personal_info_id';

        $stmt = $this->pdo->prepare($preparedSql);

        $stmt->execute([':personal_info_id' => (int) $personalInfoId]);

        return $this;
    }

    /**
     * Returns the list of personal_data (aka cv).
     *
     * @param int $userId
     *
     * @throws \PDOException
     */
    public function loadCvData($userId)
    {
        $queryStr = 'SELECT PI.id AS id, PI.cv_name AS cv_name, PI.first_name AS first_name, '
                    . 'PI.last_name AS last_name, PI.email AS email, PI.skype AS skype, '
                    . 'PI.contact_phone AS contact_phone, L.language_long_name AS language_long_name '
                    . 'FROM `' . self::TABLE_PERSONAL_INFO . '` AS `PI` '
                    . 'LEFT JOIN `' . self::TABLE_LOCALES . '` AS `L` '
                    . 'ON `PI`.locale_id = `L`.id '
                    . 'WHERE user_id = :user_id';

        $stmt = $this->pdo->prepare($queryStr);

        if ( $stmt->execute(['user_id' => $userId]) ) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    /**
     * Returns personal Info record from DB.
     *
     * @param int   $cvId
     *
     * @return Array
     * @throws \PDOException
     */
    public function getPersonalInfo($cvId)
    {
        $cvId = (int) $cvId;

        $queryStr = 'SELECT * FROM `' . self::TABLE_PERSONAL_INFO . '` AS `PI` WHERE PI.id = :cvId LIMIT 1';

        $stmt = $this->pdo->prepare($queryStr);

        $stmt->execute([':cvId' => $cvId]);

        if ( $stmt->rowCount() > 0 ) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC)[0];
        } else {
            \App::Abort(404, 'CV is not found');
        }
    }

    /**
     * Returns an array of skills related to definite Personal Info.
     *
     * @param int   $cvId
     *
     * @return Array
     */
    public function getSkills($cvId)
    {
        $cvId = (int) $cvId;

        $queryStr = 'SELECT * FROM `' . self::TABLE_SKILLS . '` AS `S` WHERE S.personal_info_id = :cvId';

        $stmt = $this->pdo->prepare($queryStr);

        if ( $stmt->execute([':cvId' => $cvId]) ) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    /**
     * Returns an array of education related to definite Personal Info (cvData)
     *
     * @param int   $cvId
     *
     * @return mixed
     */
    public function getEducation($cvId)
    {
        $cvId = (int) $cvId;

        $queryStr = 'SELECT * FROM `' . self::TABLE_EDUCATION . '` AS `E` WHERE E.personal_info_id = :cvId';

        $stmt = $this->pdo->prepare($queryStr);

        if ( $stmt->execute([':cvId' => $cvId]) ) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    /**
     * Returns an array of work experience for definite Personal Info (aka CV or CV Data)
     *
     * @param $cvId
     *
     * @return array
     */
    public function getWorkExperience($cvId)
    {
        $cvIs = (int) $cvId;

        $queryStr = 'SELECT * FROM `' . self::TABLE_WORK_EXPERIENCE . '` AS `W` WHERE W.personal_info_id = :cvId';

        $stmt = $this->pdo->prepare($queryStr);

        if ( $stmt->execute([':cvId' => $cvId]) ) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    /**
     * Deletes 1 record from DB.
     *
     * @param int   $cvId
     *
     * @return bool
     */
    public function deleteCvData($cvId)
    {
        $cvId = (int) $cvId;

        $queryStr = 'DELETE FROM `' . self::TABLE_PERSONAL_INFO .'` '
            . 'WHERE id = :id '
            . 'LIMIT 1';

        $stmt = $this->pdo->prepare($queryStr);

        $stmt->execute([':id' => $cvId]);

        if ( $stmt->rowCount() > 0 ) {
            $ret['message']     = '';
            $ret['isDeleted'] = true;
        } else {
            $ret['isDeleted'] = false;
            $ret['message'] = 'Unable to delete selected CV record';
        }

        return $ret;
    }

    /**
     * Returns all records related to definite personalId
     *
     * @param int           $personalInfoId
     * @param string        $tableName
     *
     * @return array
     */
    protected function getRelatedRecords($personalInfoId, $tableName)
    {
        $preparedSql = 'SELECT * FROM `' . $tableName . '` WHERE personal_info_id = :personal_info_id';

        $stmt = $this->pdo->prepare($preparedSql);
        $stmt->execute([':personal_info_id' => (int) $personalInfoId]);

        return $stmt->fetchAll();
    }

    /**
     * Verifies whether CV belongs to the user.
     * @param int   $personalInfoId
     * @param int   $userId
     *
     * @return bool
     */
    public function cvBelongsToUser($personalInfoId, $userId)
    {
        $personalInfoId = (int) $personalInfoId;
        $userId = (int) $userId;

        $queryStr = 'SELECT * from `' . self::TABLE_PERSONAL_INFO . '` '
            . 'WHERE id = :personal_info_id '
            . 'AND user_id = :user_id';

        $stmt = $this->pdo->prepare($queryStr);

        $stmt->execute([':personal_info_id' => $personalInfoId, ':user_id' => $userId]);

        return ($stmt->rowCount() > 0);
    }
}
