<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/1/14
 * Time: 10:08 AM
 */

namespace CvLib\Repository\CvData;

use Illuminate\Support\ServiceProvider;

class CvDataRepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('CvLib\Repository\CvData\CvDataRepositoryInterface', function() {
            //get concrete repository depending on config
            $cvDataRepositoryFactory = new CvDataRepositoryFactory();
            $cvRepo = $cvDataRepositoryFactory->getInstance();

            return $cvRepo;
        });
    }
} 