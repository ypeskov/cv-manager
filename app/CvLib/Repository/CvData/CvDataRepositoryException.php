<?php
/**
 * Project: cv-manager
 * Date: 28.01.2015
 * Time: 21:51
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

namespace CvLib\Repository\CvData;

//use \Exception as Exception;

class CvDataRepositoryException extends \Exception
{
    const NOT_FOUND                 = 4001;
    const ERROR_EXECUTE_SQL         = 4002;
    const NOT_UPDATED               = 4003;
    const TRANSACTION_NOT_STARTED   = 4004;

    public function __construct($message="", $code=0)
    {
        parent::__construct($message, $code);

        $this->message  = $message;
        $this->code     = $code;
    }
}