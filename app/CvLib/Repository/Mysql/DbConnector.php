<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/1/14
 * Time: 11:56 AM
 */

namespace CvLib\Repository\Mysql;

/**
 * Class DbConnector singleton for connecting to Mysql DB.
 * Works via PDO
 *
 * @package CvLib\Repository\Mysql
 */
class DbConnector 
{
    private $dbName;
    private $dbHost;
    private $dbUser;
    private $dbPassword;

    private static $instance;

    private $pdo;

    private function __construct()
    {
        $this->prepareDbInfo();
        $this->connect();
    }

    private function __clone() {}

    /**
     * connect to DB and save connection to $this->pdo
     *
     * @throws \Exception
     * @throws \PDOException
     */
    protected function connect()
    {
        try {
            $this->pdo = new \PDO('mysql:host=' . $this->dbHost . ';dbname=' . $this->dbName .';charset=utf8',
                                        $this->dbUser, $this->dbPassword);

            //we want get explicit errors
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch(\PDOException $e) {
            throw $e;
            //TODO: process somehow exception
        }
    }

    /**
     * set object's properties for connecting to DB
     */
    protected function prepareDbInfo()
    {
        $cvDataConfig   = \Config::get('cvData');

        $this->dbName       = $cvDataConfig['dbName'];
        $this->dbHost       = $cvDataConfig['dbHost'];
        $this->dbUser       = $cvDataConfig['dbUser'];
        $this->dbPassword   = $cvDataConfig['dbPassword'];
    }

    public function getPDO()
    {
        return $this->pdo;
    }

    public static function instance()
    {
        if ( self::$instance instanceof self ) {
            return self::$instance;
        } else {
            self::$instance = new self();
        }

        return self::$instance;
    }
} 