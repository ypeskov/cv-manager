<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/1/14
 * Time: 1:07 PM
 */

namespace CvLib\Repository\Mysql;

//TODO:  At this moment this class is not used.
//TODO:  For the current task concrete repository abstraction level is enough


class QueryExecutor 
{
    protected $pdo;
    protected $preparedQuery;
    protected $bindedParams;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function prepareQueryWithNamedParams($queryStr, Array $params)
    {
        $this->preparedQuery = $queryStr;

    }
} 