<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/4/14
 * Time: 9:07 AM
 */

namespace CvLib\Repository;


abstract class AbstractRepositoryFactory
{
    protected $repositoriesEngineMap;

    const ENGINE_MYSQL      = 'mysql';
    const ENGINE_MONGODB    = 'mongodb';
    const ENGINE_ELOQUENT   = 'eloquent';

    public function __construct()
    {
        //build a map of supported engines and repository classes
        $this->setEnginesMap();
    }


    /**
     * @return \CvLib\Repository\CvData\CvDataRepositoryInterface instance
     */
    public function getInstance()
    {
        $dataEngine = \Config::get('cvData.engine');

        //check whether config engine is correct
        if ( !array_key_exists($dataEngine, $this->repositoriesEngineMap)) {
            \App::abort(500, '[' . $dataEngine . '] '
                            . \Lang::get('app.is_not_supported_for the repository') . ':'
                            . __FILE__ . ' ' . __LINE__);
        }

        $repoName = $this->nameSpace . '\\' . $this->repositoriesEngineMap[$dataEngine];

        return new $repoName();
    }
} 