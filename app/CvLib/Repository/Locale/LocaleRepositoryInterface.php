<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/4/14
 * Time: 9:01 AM
 */

namespace CvLib\Repository\Locale;


interface LocaleRepositoryInterface
{
    public function getLocales();

    public function getLocalesWithLanguage($nameFormat);
} 