<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/4/14
 * Time: 9:04 AM
 */

namespace CvLib\Repository\Locale;

use Illuminate\Support\ServiceProvider;

class LocaleServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('cvLocale', function() {
            $localeRepositoryFactory = new LocaleRepositoryFactory();
            $localeRepo = $localeRepositoryFactory->getInstance();

            return $localeRepo;
        });
    }
} 