<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/4/14
 * Time: 9:03 AM
 */

namespace CvLib\Repository\Locale;

use CvLib\Repository\AbstractRepositoryFactory;

class LocaleRepositoryFactory extends AbstractRepositoryFactory
{

    /**
     * As this class will be initialized by parent abstract method let's define the current namespace
     * for finding this class.
     *
     * @var string
     */
    protected $nameSpace = __NAMESPACE__;

    /**
     * Maps supported DB engines and appropriate concrete repository class.
     *
     * @return void
     */
    protected function setEnginesMap()
    {
        $this->repositoriesEngineMap = [
            self::ENGINE_MYSQL      => 'LocaleRepositoryMysql',
            self::ENGINE_MONGODB    => 'LocaleMongodbRepository',
            self::ENGINE_ELOQUENT   => 'LocaleRepositoryEloquent',
        ];
    }
} 