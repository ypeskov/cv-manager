<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/4/14
 * Time: 9:03 AM
 */

namespace CvLib\Repository\Locale;

use \CvLib\Repository\Mysql\DbConnector;

class LocaleRepositoryEloquent implements LocaleRepositoryInterface
{
    public function __construct()
    {
    }

    /**
     * Returns all locales that are defined in DB.
     *
     * @return array
     * @throws \PDOException
     */
    public function getLocales()
    {
        throw new \Exception('NOT IMPLEMENTED: ' . __CLASS__ . ' ' . __METHOD__);
    }

    /**
     * Returns the list of locales with their name (short or long)
     *
     * @param string $nameFormat
     *
     * @return array
     */
    public function getLocalesWithLanguage($nameFormat='short')
    {
        $nameField =  ($nameFormat === 'long') ? 'language_long_name' : 'language_short_name';

        $cvLocales = \Locale::all(['id', $nameField])->toArray();

        $locales = [];
        foreach($cvLocales as $locale) {
            $locales[(int) $locale['id']] = $locale[$nameField];
        }

        return $locales;
    }
} 