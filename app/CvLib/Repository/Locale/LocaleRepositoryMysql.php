<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/4/14
 * Time: 9:03 AM
 */

namespace CvLib\Repository\Locale;

use \CvLib\Repository\Mysql\DbConnector;

class LocaleRepositoryMysql implements LocaleRepositoryInterface
{
    /**
     * PDO DB handler.
     *
     * @var
     */
    protected $pdo;

    const LOCALES_TABLE_NAME = 'locales';

    public function __construct()
    {
        $this->pdo = DbConnector::instance()->getPDO();
    }

    /**
     * Returns all locales that are defined in DB.
     *
     * @return array
     * @throws \PDOException
     */
    public function getLocales()
    {
        $sql = 'SELECT * from ' . self::LOCALES_TABLE_NAME;

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getLocalesWithLanguage($nameFormat='short')
    {
        $localesList = $this->getLocales();

        $locales = [];
        foreach($localesList as $locale) {
            if ( $nameFormat === 'long' ) {
                $locales[$locale['id']] = $locale['language_long_name'];
            } else {
                $locales[$locale['id']] = $locale['language_short_name'];
            }

        }

        return $locales;
    }
} 