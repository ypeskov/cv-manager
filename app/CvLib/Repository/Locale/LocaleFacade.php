<?php
/**
 * Author: Yuriy Peskov
 * Email: yuriy.peskov@gmail.com
 * Date: 11/4/14
 * Time: 3:05 PM
 */

namespace CvLib\Repository\Locale;

use Illuminate\Support\Facades\Facade;

class LocaleFacade extends Facade
{
    public static function getFacadeAccessor() { return 'cvLocale'; }
} 