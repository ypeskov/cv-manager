<?php

namespace App\lib\Response;

class Response
{
    const CODE_SUCCESS          = 0;
    const CODE_GENERAL_ERROR    = 1000;
    const CODE_USER_ADD_ERROR   = 2000;

    protected $responseCode;

    protected $isSuccess;

    protected $responseMsg;

    protected $responseData;


    public function __construct()
    {
        $this->responseCode = self::CODE_SUCCESS;
        $this->isSuccess    = true;
        $this->responseMsg  = '';
        $this->responseData = null;
    }

    /**
     * Sets the array with response data
     *
     * @param array $data
     *
     * @return $this
     */
    public function setData(Array $data)
    {
        $this->responseData = $data;

        return $this;
    }

    public function setMessage($msg)
    {
        $this->responseMsg = $msg;

        return $this;
    }

    public function setResponseCode($code)
    {
        $this->responseCode = $code;

        return $this;
    }

    public function setSuccessStatus($status, $code=null)
    {
        $this->isSuccess = (bool) $status;

        if ( $this->isSuccess !== true ) {
            $this->responseCode = ($code !== null) ? $code : self::CODE_GENERAL_ERROR;
        }

        return $this;
    }

    /**
     * Returns an array with the current data.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'code'      => $this->responseCode,
            'isSuccess' => $this->isSuccess,
            'message'   => $this->responseMsg,
            'data'      => $this->responseData,
        ];
    }
}