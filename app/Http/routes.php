<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as'    => 'homeRoute',
    'uses'  => 'HomeController@indexAction',
]);

Route::get('/get-token', function() {
    return csrf_token();
});

Route::post('/user/login', [
    'as'    => 'userLogin',
    'uses'  => 'UserController@loginAction',
]);

Route::get('/cv/view/{cvId}/{format}', [
    'as'    => 'cvView',
    'uses'  => 'CvController@viewCv',
])
    ->where('cvId', '[0-9]+');

Route::resource('user', 'UserController');

//Route::resource('user', 'UserController');

Route::group(['middleware' => 'auth'], function() {
    Route::post('/user/logout', [
        'as'    => 'userLogout',
        'uses'  => 'UserController@logoutAction',
    ]);

    Route::resource('cv', 'CvController');

    Route::resource('locale', 'LocaleController');
});
