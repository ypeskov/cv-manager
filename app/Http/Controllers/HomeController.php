<?php namespace App\Http\Controllers;

use \Input;
use \Lang;
use \Auth;

class HomeController extends BaseController
{
    const LANG_PATH = '/resources/lang/';
    const CLIENT_APP_FILE_RESOURCES = 'client.php';

    public function indexAction()
    {
        $locale = Input::get('lang', 'en-us');

        $currentDir = getcwd();
        $clientAppDirName = 'client_app';

        $textResources = json_encode(require(base_path()
            . self::LANG_PATH
            . str_replace('-', '/', $locale)
            . '/'
            . self::CLIENT_APP_FILE_RESOURCES
        ));

        if ( $user = Auth::user() ) {
            $user = $user->toArray();
        } else {
            $user = [];
        }
//        var_dump($user);

        ob_start(null, 0, PHP_OUTPUT_HANDLER_STDFLAGS);

        require($currentDir . '/' . $clientAppDirName . '/index.html');

        $html = ob_get_clean();
        echo $html;

        return;
    }

}
