<?php

namespace App\Http\Controllers;

use CvLib\Cv\Validators\CvDataValidatorException;
use CvLib\Cv\CvData\CvDataInterface;
use CvLib\Cv\Generator\CvGeneratorFactory;

use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use \Response;
use \Auth;
use App\lib\Response\Response as ResponseStructure;
use App\Http\Requests;
use \App;
use \Lang;


class CvController extends BaseController
{
    protected $cvData;
    protected $user;

    public function __construct(CvDataInterface $cvData)
    {
        parent::__construct();

        $this->cvData = $cvData;

        //verify that a user is authenticated otherwise fail
        if ( Auth::check() ) {
            $this->user = Auth::user()->toArray();
        } else {
            App::abort(403);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //user should be authorized by Auth middleware at this moment
        $userId = Auth::user()->id;

        //get CVs by the current user
        $cvList = $this->cvData->loadCvData($userId);

        return Response::json((new ResponseStructure())
            ->setData($cvList)
            ->toArray()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $cvData = $request->get('cv');
            $cvData['personal_info']['user_id'] = $this->user['id'];

            $cv = $this
                ->cvData
                ->setData($cvData)
                ->save()
                ->toArray();

            $response = Response::json((new ResponseStructure())
                ->setData($cv)
                ->setMessage(\Lang::get('app.cv_saved'))
                ->toArray()
            );

        } catch(CvDataValidatorException $e) {
            $response =  Response::json((new ResponseStructure())
                            ->setSuccessStatus(false)
                            ->setData($e->getErrors())
                            ->setMessage($e->getMessage())
                            ->toArray()
                    );
        } catch(\Exception $e) {
            $response =  Response::json((new ResponseStructure())
                ->setSuccessStatus(false)
                ->setMessage($e->getMessage())
                ->toArray()
            );
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if ( ! $this->cvData->cvBelongsToUser($id, $this->user['id']) ) {
            App::abort(403);
        }

        $cv = $this->cvData->getCvInfo($id);

        $ret = [
            'personal_info' => $cv['personal_info'],
            'skills'        => $cv['skills'],
            'educations'    => $cv['educations'],
            'workExperiences'=>$cv['work_experiences'],
        ];

        return Response::json((new ResponseStructure())
            ->setData($ret)
            ->toArray()
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ( ! $this->cvData->cvBelongsToUser($id, $this->user['id']) ) {
            App::abort(403);
        }

        $cvNewData = $request->get('cv', null);
        $cvNewData['personal_info']['user_id'] = $this->user['id'];

        try {
            $this->cvData
                ->setData($cvNewData)
                ->update($id)
                ->toArray();
        } catch(Exception $e) {
            return Response::json((new ResponseStructure())
                ->setSuccessStatus(false)
                ->setMessage($e->getMessage())
                ->toArray()
            );
        }

        return Response::json((new ResponseStructure())
                ->setMessage(Lang::get('cv_updated'))
                ->toArray()
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ( ! $this->cvData->cvBelongsToUser($id, $this->user['id']) ) {
            App::abort(403);
        }

        if ( !$this->cvData->deleteCvData($id) ) {
            $response= (new ResponseStructure())
                ->setSuccessStatus(false)
                ->setMessage(Lang::get('unable_delete_cv'))
                ->toArray()
            ;
        } else {
            $response = (new ResponseStructure())
                ->setMessage(Lang::get('cv_deleted'))
                ->toArray()
            ;
        }

        return Response::json($response);
    }

    public function viewCv($cvId, $format)
    {
        if ( ! $this->cvData->cvBelongsToUser($cvId, $this->user['id']) ) {
            App::abort(403);
        }

        if ( ! CvGeneratorFactory::isFormatAvailable($format) ) {
            App::abort(500, Lang::get('unsupported_format'));
        }

        $cvGenerator = CvGeneratorFactory::instance($format);

        return $cvGenerator->getOutput($this->cvData->getCvInfo($cvId));
    }
}
