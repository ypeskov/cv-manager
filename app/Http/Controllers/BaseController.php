<?php
namespace App\Http\Controllers;

class BaseController extends Controller
{
    /**
     * @var array Contains variables to be passed to template
     */
    protected $templateVars = [];

    protected $isDebug = false;

    public function __construct()
    {
        $this->isDebug = \Config::get('app.debug');
    }

}
