<?php namespace App\Http\Controllers;

use CvLib\Cv\CvData\CvDataInterface;
use CvLib\Cv\Validators\CvDataValidatorException;
use CvLib\Cv\Generator\CvGeneratorFactory;

use \Session;
use \cvLocale;
use \Response;
use \View;
use \Input;
use \Redirect;
use \Lang;

class CvController extends BaseController {
    /**
     * @var CvLib\Cv\CvData\CvDataInterface  CvData service for saving/reading/generating
     */
    protected $cvData;

    /**
     * @param CvDataInterface $cvData
     */
    public function __construct(CvDataInterface $cvData)
    {
        parent::__construct();

        $this->cvData = $cvData;
    }

    /**
     * Displays a form for entering new CV data.
     *
     * @return mixed
     * @throws Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function cvNewForm()
    {
        $locales = cvLocale::getLocalesWithLanguage('long');

        $tplName = 'manage.cv_form';
        $this->templateVars['menuItems']['cvNewForm']['display'] = false;

        $data = [
            'locales'           => $locales,
            'cvInfo'            => \CvLib\Cv\CvData\CvData::getEmptyCvEntity(),
        ];

        $data['cvInfo']['personal_info']['user_id'] = Session::get('user')['id'];
        $data['lastSkillIndex']                     = count($data['cvInfo']['skills']) - 1;
        $data['lastEducationIndex']                 = count($data['cvInfo']['education']) - 1;
        $data['lastWorkExperienceIndex']            = count($data['cvInfo']['workExperience']) - 1;
        $data['lastOtherInfoIndex']                 = count($data['cvInfo']['other_info']) - 1;

        //action for the form to add cv
        $data['formAction'] = 'CvController@cvAdd';

        if ( View::exists($tplName) ) {
            return Response::view($tplName, ['data' => $this->addTplVars($data)]);
        } else {
            App::abort(500, 'manage\cv_form template is not found');
        }
    }

    public function cvEditForm($cvId)
    {
        if ( !$this->cvData->cvBelongsToUser($cvId, Session::get('user')['id'] ) ) {

            return Redirect::route('homeRoute')
                ->with('flashWarning', mb_ucfirst(Lang::get('app.access_denied')));
        }

        $cvInfo = $this->cvData->getCvInfo($cvId);

        //variable to be passed to template
        $data = [];
        $data['locales']= cvLocale::getLocalesWithLanguage('long');
        $data['cvInfo'] = $cvInfo;
        $data['cvId']   = $cvId;
        $data['isEdit'] = true;

        $data['lastSkillIndex']         = count($data['cvInfo']['skills']) - 1;
        $data['lastEducationIndex']     = count($data['cvInfo']['education']) - 1;
        $data['lastWorkExperienceIndex']= count($data['cvInfo']['workExperience']) - 1;
        $data['lastOtherInfoIndex']     = count($data['cvInfo']['other_info']) - 1;

        //action for the form to edit
        $data['formAction'] = 'CvController@cvUpdate';

        $data['errors'] = Session::get('errors', []);

        if ( !empty($cvInfo) ) {
            return Response::view('manage.cv_form', ['data' => $this->addTplVars($data)]);
        } else {
            App::abort(500, 'Error getting CV information');
        }
    }

    /**
     * Action for storing data. If not valid then raise an error.
     */
    public function cvAdd()
    {
        $cvData = Input::all();

        try {
            $this->cvData
                ->setData($cvData)
                ->save();

            return Redirect::route('cvManageRoute')
                ->with('flashMessage', Lang::get('app.your_cv_has_been_saved'));
        } catch(CvDataValidatorException $e) {
            echo "Achtung! Erroren!!!";
        }
    }

    /**
     * Try to update CV data in storage.
     *
     * @return mixed
     */
    public function cvUpdate()
    {
        $cvData = Input::all();

        if ( !$this->cvData->cvBelongsToUser($cvData['personal_info_id'],
                                            Session::get('user')['id'] ) ) {

            return Redirect::route('homeRoute')
                ->with('flashWarning', mb_ucfirst(Lang::get('app.access_denied')));
        }

        try {
            $this->cvData
                ->setData($cvData)
                ->update($cvData['personal_info_id'], $cvData);

            return Redirect::route('cvManageRoute')
                ->with('flashMessage', Lang::get('app.your_cv_has_been_updated'));
        } catch(CvDataValidatorException $e) {
                return Redirect::route('cvEditFormRoute', ['cvId' => $cvData['personal_info_id']])
                    ->withErrors($e->getErrors());
        } catch(Exception $e) {
            if ( $this->isDebug === true ) {
                var_dump('File: ' . $e->getFile());
                var_dump('Line: ' . $e->getLine());
                var_dump('Message' . $e->getMessage());
                var_dump('Code: ' . $e->getCode());
                die();
            }

            return Redirect::route('homeRoute')
                ->with('flashWarning', Lang::get('app.couldnt_update_cv'));
        }
    }

    /**
     * @return mixed
     */
    public function cvManage()
    {
        if ( !\Session::get('isLoggedIn') or !isset(\Session::get('user')['id']) ) {
            return \Redirect::route('homeRoute')
                ->with('flashWarning', \mb_ucfirst(\Lang::get('app.you should be authenticated.')));
        }

        $tplName = 'manage.cv_list';
        $this->templateVars['menuItems']['cvManage']['display'] = false;

        $cvList = $this->cvData->loadCvData(\Session::get('user')['id']);

        $vars['available_cv'] = $cvList;

        if ( \View::exists($tplName) ) {
            return \Response::view($tplName, ['data' => $this->addTplVars($vars)]);
        } else {
            \App::abort(500, 'manage\cv_list template is not found');
        }

        return $cvList;
    }

    /**
     * @return \Response::view()
     */
    public function cvShow()
    {
        $cvId   = Input::get('cvId', null);
        $format = Input::get('format', null);

        if ( $cvId === null or $format === null ) {
            App::abort(400, 'Invalid parameter/parameters');
        }

        if ( !$this->cvData->cvBelongsToUser($cvId, Session::get('user')['id'] ) ) {

            return Redirect::route('homeRoute')
                ->with('flashWarning', mb_ucfirst(Lang::get('app.access_denied')));
        }

        $data = [];
        $data['cvInfo'] = $this->cvData->getCvInfo($cvId);

        if ( !empty($data['cvInfo']) ) {
            $data = $this->addTplVars($data['cvInfo']);

            $cvGenerator = CvGeneratorFactory::instance($format);

            return $cvGenerator->getOutput($data);
        } else {
            App::abort(500, 'Error getting CV information');
        }
    }
}
