<?php
namespace App\Http\Controllers;

use \Input;
use Mockery\CountValidator\Exception;
use \Session;
use \Auth;
use \Response;
use \Request;
use \User;
use \App;
use \App\lib\Response\Response as ResponseStructure;


class UserController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $newUser = Request::get('user', null);

        if ( $newUser !== null ) {
            $user = new User();

            if ( $user->validate($newUser) ) {
                $user->email    = $newUser['email'];
                $user->password = \Hash::make($newUser['password']);

                try {
                    $user->save();
                } catch(Exception $e) {
                    App::abort(500);
                }
            } else {
                $fullError = '';
                foreach($user->getErrors()->toArray() as $error) {
                    $fullError .= implode(' ', $error) . " ";
                }

                return Response::json((new ResponseStructure())
                    ->setSuccessStatus(false, ResponseStructure::CODE_USER_ADD_ERROR)
                    ->setMessage($fullError)
                    ->setData($user->getErrors()->toArray())
                    ->toArray()
                );
            }
        }

        return Response::json((new ResponseStructure())
            ->setData($user->toArray())
            ->toArray()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        var_dump(Route::currentRouteName());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function loginAction()
    {
        $inputUser = Input::all();

        $user = [
            'email'     => $inputUser['username'],
            'password'  => $inputUser['password']
        ];

        if ( Auth::attempt($user) ) {
            Session::put('isLoggedIn', true);

            $userAuthed = Auth::user()->toArray();
            Session::put('user', $userAuthed);

            return Response::json(
                (new ResponseStructure())
                    ->setData($userAuthed)
                    ->toArray()
            );
        } else {
            App::abort(403);
        }
    }

    public function logoutAction()
    {
        $ret = null;

        try {
            Auth::logout();

            $ret = Response::json(
                (new ResponseStructure())
                    ->toArray()
            );
        } catch(Exception $e) {
            $ret = Response::json(
                (new ResponseStructure())
                    ->toArray()
            );
        }

        return $ret;
    }
}
