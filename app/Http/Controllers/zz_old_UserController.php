<?php namespace App\Http\Controllers;

/**
 * Project: cv-manager
 * Date: 01.02.2015
 * Time: 13:01
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com>.
 */

use \Session;
use \Response;
use \Redirect;
use \User;
use \Lang;
use \Input;
use \Hash;
use \Auth;


class old_UserController extends BaseController
{
    public function login()
    {
        $inputUser = \Input::all();

        $user = [
            'email'     => $inputUser['email'],
            'password'  => $inputUser['password']
        ];

        if ( \Auth::attempt($user) ) {
            \Session::put('isLoggedIn', true);
            \Session::put('user', \Auth::user()->toArray());

            return \Redirect::route('homeRoute')
                ->with('flashMessage', \mb_ucfirst(\Lang::get('app.You are logged in.')));
        }

        return \Redirect::route('homeRoute')
            ->with('flashWarning', \mb_ucfirst(\Lang::get('app.Incorrect email/password.')));
    }

    /**
     * Logs a user out of account
     *
     * @return mixed
     */
    public function logout()
    {
        if ( Session::has('user') and Auth::check() ) {
            Session::set('isLoggedIn', false);
            Session::forget('user');
            Auth::logout();

            return Redirect::route('homeRoute')
                ->with('flashMessage', mb_ucfirst(Lang::get('app.you are logged out.')));
        } else {
            return Redirect::route('homeRoute')
                ->with('flashWarning', mb_ucfirst(Lang::get('app.You are not logged in.')));
        }

    }

    /**
     * Displays a for for registering a new user.
     *
     * @return mixed
     */
    public function registerForm()
    {
        $tplName = 'user.register_user';

        $data['errors'] = Session::get('errors', []);

        return Response::view($tplName, ['data' => $this->addTplVars($data)]);
    }

    /**
     * Try to register a new user. If success then redirect to home page (without logging in).
     *
     * @return mixed
     */
    public function registerUser()
    {
        $user = new User();

        $data['username'] = Input::get('user_email', null);
        $data['email']    = Input::get('user_email', null);
        $data['password'] = Input::get('user_password', null);
        $data['password_confirmation'] = Input::get('user_password_confirmation', null);

        if ( $user->validate($data) ) {
            $user->username = $data['username'];
            $user->email    = $data['email'];
            $user->password = Hash::make($data['password']);

            $user->save();
        } else {
            return Redirect::route('registerFormRoute')->withErrors($user->getErrors());
        }


        return Redirect::route('homeRoute')
            ->with('flashMessage', mb_ucfirst(Lang::get('app.you_have_been_registered')));
    }

    public function changePasswordForm()
    {
        $tplName = 'user.change_password';

        $data['errors'] = Session::get('errors', []);

        return Response::view($tplName, ['data' => $this->addTplVars($data)]);
    }

    public function applyNewPassword()
    {
        $response = Redirect::route('changePasswordFormRoute');

        $user = (new User())->find(Session::get('user')['id'], null);

        if ( !(is_object($user) and $user instanceof user) ) {
            $response = $response->with('flashWarning', mb_ucfirst(Lang::get('app.access_denied')));
        }

        $data['oldPassword']            = Input::get('old_password', null);
        $data['password']               = Input::get('user_password', null);
        $data['password_confirmation']  = Input::get('user_password_confirmation', null);

        if ( Hash::check($data['oldPassword'], $user->password) ) {
            if ( $user->validatePassword($data) ) {
                $user->password = Hash::make($data['password']);

                if ( $user->save() ) {
                    $response = Redirect::route('homeRoute')
                        ->with('flashMessage', mb_ucfirst(Lang::get('app.password_has_been_changed')));
                } else {
                    $response = $response
                        ->with('flashWarning', mb_ucfirst(Lang::get('app.error_change_password')));
                }
            } else {
                $response = $response->withErrors($user->getErrors());
            }
        } else {
            $response = $response->with('flashWarning', mb_ucfirst(Lang::get('app.incorrect_old_password')));
        }

        return $response;
    }
}