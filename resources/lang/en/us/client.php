<?php

return [
    'email'     => 'email',
    'password'  => 'password',
    'login'     => 'login',

    'welcome'   => 'welcome',

    'Welcome to the site that will help you generate, store and manage Your CV in different formats.'
        => 'Welcome to the site that will help you generate, store and manage Your CV in different formats.',

    'At this moment HTML and Pdf formats are supported. More formats are planned in future: Word, Image.'
        => 'At this moment HTML and Pdf formats are supported. More formats are planned in future: Word, Image.',

    'We are also planning to add possibility to add possibility to send CV right from the site.'
        => 'We are also planning to add possibility to add possibility to send CV right from the site.',

    'error_login'   => 'login failed',
    'logout'        => 'logout',


    'CV Manager'    => 'CV Manager',

    'dashboard'     => 'dashboard',
    'home'          => 'home',
    'manage CV'     => 'manage CV',
    'new CV'        => 'new CV',

    'personal_information'  => 'personal information',
    'language'      => 'language',
    'cv_name'       => 'CV name',
    'first_name'    => 'first name',
    'last_name'     => 'last name',
    'birth_date'    => 'birth date',
    'skype'         => 'skype',
    'contact_phone' => 'contact phone',
    'skill_group'   => 'skill group',
    'skill_value'   => 'skill value',
    'skills'        => 'skills',
    'remove'        => 'remove',
    'education'     => 'education',
    'institution'   => 'institution',
    'end_date'      => 'end date',
    'start_date'    => 'start date',
    'add_education' => 'add education',
    'more_skills'   => 'more skills',
    'speciality'    => 'speciality',
    'work experience'=>'work experience',
    'company_name'  => 'company name',
    'position'      => 'position',
    'description'   => 'description',
    'add_work'      => 'add work',
    'remove_work_experience'    => 'remove work experience',
    'save_cv'       => 'save cv',
    'language_required'     => 'language required',
    'cv_name_required'      => 'cv name required',
    'first_name_required'   => 'first name required',
    'email_required'        => 'email required',
    'please_check_errors'   => 'please check errors',

    'sure_want_delete_this_cv?' => 'delete this cv?',
    'cv_id'                 => 'cv ID',
    'cv_information'        => 'cv information',
    'cv_control'            => 'cv control',
    'edit'                  => 'edit',
    'view_html'             => 'view HTML',
    'view_pdf'              => 'view PDF',
    'delete'                => 'delete',
];