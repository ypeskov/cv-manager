<?php

return [
    'email'     => 'Ваш email',
    'password'  => 'Пароль',
    'login'     => 'вход',

    'welcome'   => 'добро пожаловать',

    'Welcome to the site that will help you generate, store and manage Your CV in different formats.'
    => 'Добро пожаловать на сайт, который поможет вам создать, сохранить и управлять Вашими резюме',

    'At this moment HTML and Pdf formats are supported. More formats are planned in future: Word, Image.'
    => 'В настоящий момент поддерживаются форматы HTML и PDF. Планируется больше форматов в будущем: MS Word, изображения',

    'We are also planning to add possibility to add possibility to send CV right from the site.'
    => 'Также в планах добавление возможности отправки резюме на почту прямо с сайта.',

    'logout'        => 'выход',

    'error_login'   => 'Ошибка входа',

    'CV Manager'    => 'менеджер резюме',

    'dashboard'     => 'начало',
    'home'          => 'домой',
    'manage CV'     => 'управлять резюме',
    'new CV'        => 'новое резюме',

    'personal_information'  => 'личная информация',
    'language'      => 'язык',
    'cv_name'       => 'название резюме',
    'first_name'    => 'имя',
    'last_name'     => 'фамилия',
    'birth_date'    => 'дата рождения',
    'skype'         => 'skype',
    'contact_phone' => 'контактный телефон',
    'skill_group'   => 'группа навыков',
    'skill_value'   => 'навыки в группе',
    'skills'        => 'навыки',
    'remove'        => 'удалить',
    'education'     => 'образование',
    'institution'   => 'учебное заведение',
    'end_date'      => 'дата окончания',
    'start_date'    => 'дата начала',
    'add_education' => 'добавить образование',
    'more_skills'   => 'еще навыки',
    'speciality'    => 'специальность',
    'work experience'=>'опыт работы',
    'company_name'  => 'название компании',
    'position'      => 'должность',
    'description'   => 'описание',
    'add_work'      => 'добавить место работы',
    'remove_work_experience'    => 'удалить опыт работы',
    'save_cv'       => 'сохранить резюме',
    'language_required'     => 'язык является обязательным',
    'cv_name_required'      => 'название резюме является обязательным',
    'first_name_required'   => 'имя является обязательным',
    'email_required'        => 'email является обязательным',
    'please_check_errors'   => 'пожалуйста проверьте ошибки',

    'sure_want_delete_this_cv?' => 'удалить это резюме?',
    'cv_id'                 => 'ID резюме',
    'cv_information'        => 'информация про резюме',
    'cv_control'            => 'управление резюме',
    'edit'                  => 'редактировать',
    'view_html'             => 'просмотр HTML',
    'view_pdf'              => 'просмотр PDF',
    'delete'                => 'удалить',
];