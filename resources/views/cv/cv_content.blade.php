<!-- using css in the same file to be able to use it in PDF -->
<style>
    div, tr, table, td, th, ul, li {
        padding:    0;
        margin:     0;
    }

    li {
        list-style-position: inside;
    }

    .cv_skill_wrapper li{
        list-style: none inside none;
    }

    #cv_content_wrapper {
        width:          100%;
        border:         1px solid #000;
        border-collapse:collapse;
    }

    #cv_content_wrapper td {
        border:     1px solid #000;
    }

    .section_name {
        width:          25%;
        text-align:     right;
        vertical-align: top;
        padding:        0.5% 1% 0 0;
        border:         1px solid #000;
    }

    .section_content {
        width:      75%;
        padding:    0.5% 0.5% 0.5% 1%;
        border:     1px solid #000;
    }

    /*.cv_work_description, .cv_other_info {*/
        /*padding: 0 0 0 2%;*/
    /*}*/
</style>

<table id="cv_content_wrapper">
    <tr>
        <td class="section_name">
            <strong><?= mb_ucfirst(Lang::get('app.personal_information')) ?></strong>
        </td>

        <td class="section_content">
            <div>
                <strong><?= mb_ucfirst(Lang::get('app.name')) ?>:</strong>&nbsp;
                {{ $data['personal_info']['first_name'] }}&nbsp;{{ $data['personal_info']['last_name'] }}
            </div>
            <div>
                <strong><?= mb_ucfirst(Lang::get('app.birth_date')) ?>:</strong>&nbsp;
                {{ $data['personal_info']['birth_date'] }}
            </div>
            <div>
                <strong><?= mb_ucfirst(Lang::get('app.email')) ?>:</strong>&nbsp;
                {{ $data['personal_info']['email'] }}
            </div>
            <div>
                <strong><?= mb_ucfirst(Lang::get('app.skype')) ?>:</strong>&nbsp;
                {{ $data['personal_info']['skype'] }}
            </div>
        </td>
    </tr>

    <tr class="cv_skill_wrapper">
        <td class="section_name">
            <strong><?= mb_ucfirst(Lang::get('app.skills')) ?></strong>
        </td>

        <td class="section_content">
            <ul>
                @foreach($data['skills'] as $skill)
                    <li><strong>{{ $skill['group_name'] }}:</strong>&nbsp;{{ $skill['group_content'] }}</li>
                @endforeach
            </ul>
        </td>
    </tr>

    <tr>
        <td class="section_name">
            <strong><?= mb_ucfirst(Lang::get('app.education')) ?></strong>
        </td>

        <td class="section_content">
            @foreach($data['educations'] as $index => $institution)
                <div>
                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.institution_name')) ?></strong>:&nbsp;
                        {{ $institution['educational_inst'] }}
                    </div>

                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.date_start')) ?></strong>:&nbsp;
                        {{ $institution['date_start'] }}
                    </div>

                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.date_finish')) ?></strong>:&nbsp;
                        {{ $institution['date_finish'] }}
                    </div>

                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.specialty')) ?></strong>:&nbsp;
                        {{ $institution['specialty'] }}
                    </div>

                    <?php if ( ($index+1) < count($data['educations']) ): ?>
                    <br />
                    <?php endif ?>

                </div>
            @endforeach
        </td>
    </tr>

    @if ( count($data['work_experiences']) > 0 )
    <tr>
        <td class="section_name">
            <strong><?= mb_ucfirst(Lang::get('app.work_experience')) ?></strong>
        </td>

        <td class="section_content">
            @foreach($data['work_experiences'] as $index => $work)
                <div>
                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.company_name')) ?></strong>:&nbsp;
                        {{ $work['company_name'] }}
                    </div>

                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.date_start')) ?></strong>:&nbsp;
                        {{ $work['date_start'] }}
                    </div>

                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.date_finish')) ?></strong>:&nbsp;
                        {{ $work['date_finish'] }}
                    </div>

                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.position')) ?></strong>:&nbsp;
                        {{ $work['position'] }}
                    </div>

                    <div>
                        <strong><?= mb_ucfirst(Lang::get('app.description')) ?></strong>:&nbsp;
                        <div class="cv_work_description">{!! $work['description'] !!}</div>
                    </div>

                    <?php if ( ($index+1) < count($data['work_experiences']) ): ?>
                    <br />
                    <?php endif ?>

                </div>
            @endforeach
        </td>
    </tr>
    @endif

    @if ( count($data['other_info']) > 0 )
        <tr class="section_content">
            <td class="section_name">
                <strong><?= mb_ucfirst(Lang::get('app.other_information')) ?></strong>
            </td>

            <td class="section_content">
                @foreach($data['other_info'] as $index => $otherInfoItem)
                    <div>
                        <div>
                            <strong>{{ $otherInfoItem['info_name'] }}:</strong>&nbsp;
                            <div class="cv_other_info">{!! $otherInfoItem['info_value'] !!}</div>
                        </div>

                        <?php if ( ($index+1) < count($data['other_info']) ): ?>
                        <br />
                        <?php endif ?>

                    </div>
                @endforeach
            </td>
        </tr>
    @endif
</table>