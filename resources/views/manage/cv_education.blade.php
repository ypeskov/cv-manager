<div id="education_wrapper">

    @for($i=0; $i <= $data['lastEducationIndex']; $i++)
        <div class="education_row"
            @if ($i === $data['lastEducationIndex']) data-education_group="last" @endif
             data-education-row-order="{{ $i }}">

            <div class="edu_inst_name_label">
                <?= Form::label('education[educational_inst][]',
                        mb_ucfirst(Lang::get('app.institution_name'))); ?>
            </div>
            <div class="edu_inst_name_value">
                <?= Form::text('education[educational_inst][]',
                        $data['cvInfo']['education'][$i]['educational_inst'],
                        ['placeholder' => mb_ucfirst(Lang::get('app.institution_sample')), ]); ?>
            </div>

            <div class="edu_inst_start_label">
                <?= Form::label('education[date_start][]', mb_ucfirst(Lang::get('app.date_start'))); ?>
            </div>
            <div class="edu_inst_start_value">
                <?= Form::text('education[date_start][]',
                        $data['cvInfo']['education'][$i]['date_start'],
                        ['placeholder' => mb_ucfirst(Lang::get('app.date_start_sample')),
                         'id' => 'education_start_date_val_' . $i,
                         'class' => 'education_start_date_val', ]); ?>
            </div>

            <div class="edu_inst_finish_label">
                <?= Form::label('education[date_finish][]', mb_ucfirst(Lang::get('app.date_finish'))); ?>
            </div>
            <div class="edu_inst_finish_value">
                <?= Form::text('education[date_finish][]',
                        $data['cvInfo']['education'][$i]['date_finish'],
                        ['placeholder' => mb_ucfirst(Lang::get('app.date_finish_sample')),
                         'id' => 'education_start_finish_val_' . $i,
                         'class' => 'education_finish_date_val', ]); ?>
            </div>

            <div class="edu_inst_specialty_label">
                <?= Form::label('education[specialty][]', mb_ucfirst(Lang::get('app.specialty'))); ?>
            </div>
            <div class="edu_inst_specialty_value">
                <?= Form::text('education[specialty][]',
                        $data['cvInfo']['education'][$i]['specialty'],
                        ['placeholder' => mb_ucfirst(Lang::get('app.specialty_sample')), ]); ?>

                <a href="" class="delete_education">
                    <img src="/images/delete.png" />
                </a>
            </div>


        </div>
    @endfor

    <div id="add_education_button_wrapper">
        <?= Form::button(mb_ucfirst(Lang::get('app.add_education')), ['id' => 'add_education']); ?>
    </div>

    <div class="education_row" id="education_row_template">
        <div class="edu_inst_name_label">
            <?= Form::label('education[educational_inst][]',
                    mb_ucfirst(Lang::get('app.institution_name'))); ?>
        </div>
        <div class="edu_inst_name_value">
            <?= Form::text('education[educational_inst][]',
                    '',
                    ['placeholder' => mb_ucfirst(Lang::get('app.institution_sample')), ]); ?>
        </div>

        <div class="edu_inst_start_label">
            <?= Form::label('education[date_start][]',
                    mb_ucfirst(Lang::get('app.date_start'))); ?>
        </div>
        <div class="edu_inst_start_value">
            <?= Form::text('education[date_start][]',
                    '',
                    ['placeholder' => mb_ucfirst(Lang::get('app.date_start_sample')),
                     'class' => 'education_start_date_val',
                     'id' => '', ]); ?>
        </div>

        <div class="edu_inst_finish_label">
            <?= Form::label('education[date_finish][]',
                    mb_ucfirst(Lang::get('app.date_finish'))); ?>
        </div>
        <div class="edu_inst_finish_value">
            <?= Form::text('education[date_finish][]',
                    '',
                    ['placeholder' => mb_ucfirst(Lang::get('app.date_finish_sample')),
                     'class' => 'education_finish_date_val',
                     'id' => '', ]); ?>
        </div>

        <div class="edu_inst_specialty_label">
            <?= Form::label('education[specialty][]',
                    mb_ucfirst(Lang::get('app.specialty'))); ?>
        </div>
        <div class="edu_inst_specialty_value">
            <?= Form::text('education[specialty][]',
                    '',
                    ['placeholder' => mb_ucfirst(Lang::get('app.specialty_sample')), ]); ?>

            <a href="" class="delete_education">
                <img src="/images/delete.png" />
            </a>
        </div>
    </div>

</div>