@extends('layouts.html')

@section('content')
    @include('top_menu')

    <table id="cv_list_wrapper">
        @foreach($data['available_cv'] as $cv)
        <tr class="cv_list_row">
            <td><span class="invisible cv_id">{{ $cv['id'] }}</span>{{ $cv['language_long_name'] }}</td>
            <td>{{ $cv['cv_name'] }}</td>
            <td>{{ $cv['first_name'] }}</td>
            <td>{{ $cv['last_name'] }}</td>
            <td class="email_cell">{{ $cv['email'] }}</td>

            <td>
                <a href="{{ $data['uri']['cvShow'] }}?format=html&cvId={{ $cv['id'] }}"
                   class="btn btn-primary btn-xs">
                    <?= mb_ucfirst(Lang::get('app.view_html')) ?>
                </a>
            </td>

            <td>
                <a href="{{ $data['uri']['cvShow'] }}?format=pdf&cvId={{ $cv['id'] }}"
                   class="btn btn-primary btn-xs">
                    <?= mb_ucfirst(Lang::get('app.view_pdf')) ?>
                </a>
            </td>

            <td>
                <a href="<?= URL::route('cvEditFormRoute', ['cvId' => $cv['id']]) ?>"
                   class="cv_edit_button btn btn-primary btn-xs"><?= mb_ucfirst(Lang::get('app.edit')) ?>
                </a>
            </td>

            <td>
                <a href="" class="cv_delete_button btn btn-primary btn-xs">
                    <?= mb_ucfirst(Lang::get('app.delete')) ?>
                </a>
            </td>
        </tr>
        @endforeach
    </table>
@stop