<div id="skill_wrapper">

    @for($i=0; $i <= $data['lastSkillIndex']; $i++)
        <div class="skill_row"
             @if ($i === $data['lastSkillIndex']) data-skill_group="last" @endif
             data-skill-row-order="{{ $i }}">

            <div class="skill_group_name_label">
                <?= Form::label('skills[group_name][' . $i . ']',
                        mb_ucfirst(Lang::get('app.skill_group_name'))); ?>:
            </div>

            <div class="skill_group_name_value">
                <?= Form::text('skills[group_name][' . $i . ']',
                        $data['cvInfo']['skills'][$i]['group_name'],
                        ['placeholder' => mb_ucfirst(Lang::get('app.skill_group_sample')),
                         'class' => 'skill_name_input', ]); ?>
            </div>

            <div class="skill_group_content_label">
                <?= Form::label('skills[group_content][' . $i . ']',
                        mb_ucfirst(Lang::get('app.skill_group_content'))); ?>:&nbsp;
            </div>

            <div class="skill_group_content_value">
                <?= Form::text('skills[group_content][' . $i . ']',
                        $data['cvInfo']['skills'][$i]['group_content'],
                        ['placeholder' => mb_ucfirst(Lang::get('app.skill_content_sample')), ]); ?>

                <a href="" class="delete_skill">
                    <img src="/images/delete.png" />
                </a>
            </div>
        </div>
    @endfor

    <div>
        <div class="skill_group_name_label">
            <?= Form::button(mb_ucfirst(Lang::get('app.add_skills')), ['id' => 'add_skill']); ?>
        </div>
    </div>

    <div id="skill_row_template" class="skill_row">
        <div class="skill_group_name_label">
            <?= Form::label('skills[group_name][]',
                    mb_ucfirst(Lang::get('app.skill_group_name'))); ?>:
        </div>
        <div class="skill_group_name_value">
            <?= Form::text('skills[group_name][]',
                    '',
                    ['placeholder' => mb_ucfirst(Lang::get('app.skill_group_sample')),
                     'class' => 'skill_name_input', ]); ?>
        </div>

        <div class="skill_group_content_label">
            <?= Form::label('skills[group_content][]',
                mb_ucfirst(Lang::get('app.skill_group_content'))); ?>:&nbsp;
        </div>
        <div class="skill_group_content_value">
            <?= Form::text('skills[group_content][]',
                    '',
                    ['placeholder' => mb_ucfirst(Lang::get('app.skill_content_sample')), ]); ?>

            <a href="" class="delete_skill">
                <img src="/images/delete.png" />
            </a>
        </div>
    </div>
</div>