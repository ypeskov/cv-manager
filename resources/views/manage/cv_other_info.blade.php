<div id="other_info_wrapper">

    @for($i=0; $i <= $data['lastOtherInfoIndex']; $i++)
        <div class="other_info_row"
        @if ($i === $data['lastOtherInfoIndex']) data-other_info_group="last" @endif
             data-other_info-row-order="{{ $i }}">

            <div class="other_info_group_name_wrapper">
                <span class="other_info_group_name_label">
                    <?= Form::label('other_info[info_name][' . $i . ']',
                            mb_ucfirst(Lang::get('app.other_info_group_name'))); ?>:
                </span>

                <span class="other_info_group_name_value">
                    <?= Form::text('other_info[info_name][' . $i . ']',
                            $data['cvInfo']['other_info'][$i]['info_name'],
                            ['placeholder' => mb_ucfirst(Lang::get('app.other_info_group_sample')),
                             'class' => 'other_info_name_input', ]); ?>
                </span>
            </div>

            <div class="other_info_group_value_wrapper">
                <span class="other_info_group_content_label">
                    <?= Form::label('other_info[info_value][' . $i . ']',
                            mb_ucfirst(Lang::get('app.other_info_group_content'))); ?>:&nbsp;
                </span>

                <span class="other_info_group_content_value">
                    <?= Form::textarea('other_info[info_value][' . $i . ']',
                            $data['cvInfo']['other_info'][$i]['info_value'],
                            ['placeholder' => mb_ucfirst(Lang::get('app.other_info_value_sample')),
                             'class' => 'wysiwyg_area']); ?>
                </span>

                <span>
                    <a class="delete_other_info"
                       href=""><?= mb_ucfirst(Lang::get('app.remove_other_info')); ?>
                    </a>
                </span>
            </div>
        </div>
    @endfor

    <div>
        <?= Form::button(mb_ucfirst(Lang::get('app.add_other_info')), ['id' => 'add_other_info']); ?>
    </div>

    <div id="other_info_row_template" class="other_info_row">
        <div class="other_info_group_name_wrapper">
                <span class="other_info_group_name_label">
                    <?= Form::label('other_info[info_name][' . $i . ']',
                            mb_ucfirst(Lang::get('app.other_info_group_name'))); ?>:
                </span>

                <span class="other_info_group_name_value">
                    <?= Form::text('other_info[info_name][' . $i . ']',
                            '',
                            ['placeholder' => mb_ucfirst(Lang::get('app.other_info_group_sample')),
                             'class' => 'other_info_name_input', ]); ?>
                </span>
        </div>

        <div class="other_info_group_value_wrapper">
                <span class="other_info_group_content_label">
                    <?= Form::label('other_info[info_value][' . $i . ']',
                            mb_ucfirst(Lang::get('app.other_info_group_content'))); ?>:&nbsp;
                </span>

                <span class="other_info_group_content_value">
                    <?= Form::textarea('other_info[info_value][' . $i . ']',
                            '',
                            ['placeholder' => mb_ucfirst(Lang::get('app.other_info_value_sample')), ]); ?>
                </span>

                <span>
                    <a class="delete_other_info"
                       href=""><?= mb_ucfirst(Lang::get('app.remove_other_info')); ?>
                    </a>
                </span>
        </div>
    </div>
</div>