<div id="personal_info">
    <div><?= Form::hidden('personal_info_id', $data['cvInfo']['personal_info']['id']); ?></div>
    <div><?= Form::hidden('personal_info[user_id]', $data['cvInfo']['personal_info']['user_id']); ?></div>
    <div class="personal_info_row">
        <div class="personal_info_cell_label">
            <?= Form::label('locale', mb_ucfirst(Lang::get('app.language'))); ?>:
        </div>
        <div class="personal_info_cell_val">
            <?= Form::select('locale', $data['locales'], $data['cvInfo']['personal_info']['locale_id']); ?>
        </div>
    </div>

    <div class="personal_info_row">
        <div class="personal_info_cell_label">
            <?= Form::label('personal_info[cv_name]', mb_ucfirst(Lang::get('app.cv_name'))); ?>:
        </div>
        <div class="personal_info_cell_val">
            <?= Form::text('personal_info[cv_name]',
                    $data['cvInfo']['personal_info']['cv_name'],
                    ['placeholder' => mb_ucfirst(Lang::get('app.specialty_sample'))]); ?>
        </div>
    </div>

    <div class="personal_info_row">
        <div class="personal_info_cell_label">
            <?= Form::label('personal_info[first_name]', mb_ucfirst(Lang::get('app.first_name'))); ?>:
        </div>
        <div class="personal_info_cell_val">
            <?= Form::text('personal_info[first_name]',
                    $data['cvInfo']['personal_info']['first_name'],
                    ['placeholder' => mb_ucfirst(Lang::get('app.first_name')), ]); ?>
        </div>
    </div>

    <div class="personal_info_row">
        <div class="personal_info_cell_label">
            <?= Form::label('personal_info[last_name]', mb_ucfirst(Lang::get('app.last_name'))); ?>:
        </div>
        <div class="personal_info_cell_val">
            <?= Form::text('personal_info[last_name]',
                    $data['cvInfo']['personal_info']['last_name'],
                    ['placeholder' => mb_ucfirst(Lang::get('app.last_name')), ]); ?>
        </div>
    </div>

    <div class="personal_info_row">
        <div class="personal_info_cell_label">
            <?= Form::label('personal_info[birth_date]', mb_ucfirst(Lang::get('app.birth_date'))); ?>:
        </div>
        <div class="personal_info_cell_val">
            <?= Form::text('personal_info[birth_date]',
                    $data['cvInfo']['personal_info']['birth_date'],
                    ['placeholder' => mb_ucfirst(Lang::get('app.birth_date')),
                     'id' => 'personal_info_birth_date', ]); ?>
        </div>
    </div>

    <div class="personal_info_row">
        <div class="personal_info_cell_label">
            <?= Form::label('personal_info[email]', mb_ucfirst(Lang::get('app.email'))); ?>:
        </div>
        <div class="personal_info_cell_val">
            <?= Form::email('personal_info[email]',
                    $data['cvInfo']['personal_info']['email'],
                    ['placeholder' => mb_ucfirst(Lang::get('app.email')), ]); ?>
        </div>
    </div>

    <div class="personal_info_row">
        <div class="personal_info_cell_label">
            <?= Form::label('personal_info[skype]', mb_ucfirst(Lang::get('app.skype'))); ?>:
        </div>
        <div class="personal_info_cell_val">
            <?= Form::text('personal_info[skype]',
                    $data['cvInfo']['personal_info']['skype'],
                    ['placeholder' => mb_ucfirst(Lang::get('app.skype')), ]); ?>
        </div>
    </div>

    <div class="personal_info_row">
        <div class="personal_info_cell_label">
            <?= Form::label('personal_info[contact_phone]', mb_ucfirst(Lang::get('app.contact_phone'))); ?>:
        </div>
        <div class="personal_info_cell_val">
            <?= Form::text('personal_info[contact_phone]',
                    $data['cvInfo']['personal_info']['contact_phone'],
                    ['placeholder' => mb_ucfirst(Lang::get('app.contact_phone')), ]); ?>
        </div>
    </div>
</div>