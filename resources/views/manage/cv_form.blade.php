@extends('layouts.html')


@section('content')
    @include('top_menu')

    @include('user.error_messages')

    <div>
    <?= Form::open(['name' => 'cvDataForm',
                    'action' => $data['formAction'],
                    'files' => true,
                    'id' => 'cvDataForm']); ?>

    @include('manage.cv_personal_info')

    <div><strong>Skills:</strong></div>
    @include('manage.cv_skills')

    <div><strong>Education:</strong></div>
    @include('manage.cv_education')

    <div><strong>Work Experience:</strong></div>
    @include('manage.cv_work_experience')

    <div><strong><?= mb_ucfirst(Lang::get('app.other_information')); ?></strong></div>
    @include('manage.cv_other_info')

    <hr />

    <div>
        <?= Form::submit(Lang::get('app.save_cv_data')); ?>
    </div>

    <?= Form::close(); ?>
    </div>

@stop
