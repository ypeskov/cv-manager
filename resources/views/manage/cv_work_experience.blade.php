<div id="work_experience_wrapper">
    @for($i=0; $i <= $data['lastWorkExperienceIndex']; $i++)
        <div class="work_experience_row"
            @if ($i === $data['lastWorkExperienceIndex']) data-work_experience_group="last" @endif
             data-work_experience-row-order="{{ $i }}" >

            <div class="company_name_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[company_name][]', mb_ucfirst(Lang::get('app.company_name'))); ?>
                </span>

                <span>
                    <?= Form::text('work[company_name][]',
                            $data['cvInfo']['workExperience'][$i]['company_name'],
                            [
                                'placeholder' => mb_ucfirst(Lang::get('app.company_sample')),
                                'class' => 'work_experience_value'
                            ]);
                    ?>
                </span>

                <span>
                    <a class="delete_work_experience"
                       href=""><?= mb_ucfirst(Lang::get('app.remove_work_experience')); ?>
                    </a>
                </span>
            </div>

            <div class="work_start_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[date_start][]', mb_ucfirst(Lang::get('app.date_start'))); ?>
                </span>
                <span>
                    <?= Form::text('work[date_start][]',
                            $data['cvInfo']['workExperience'][$i]['date_start'],
                            [
                                'id' => 'work_experience_start_date_val_'. $i,
                                'placeholder' => mb_ucfirst(Lang::get('app.date_start_sample')),
                                'class' => 'work_experience_value work_experience_start_date_val'
                            ]);
                    ?>
                </span>
            </div>

            <div class="work_finish_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[date_finish][]', mb_ucfirst(Lang::get('app.date_finish'))); ?>
                </span>
                <span>
                    <?= Form::text('work[date_finish][]',
                            $data['cvInfo']['workExperience'][$i]['date_finish'],
                            [
                                'id' => 'work_experience_finish_date_val_'. $i,
                                'placeholder' => mb_ucfirst(Lang::get('app.date_finish_sample')),
                                'class' => 'work_experience_value work_experience_finish_date_val'
                            ]);
                    ?>
                </span>
            </div>

            <div class="work_position_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[position][]', mb_ucfirst(Lang::get('app.position'))); ?>
                </span>
                <span>
                    <?= Form::text('work[position][]',
                            $data['cvInfo']['workExperience'][$i]['position'],
                            [
                                'placeholder' => mb_ucfirst(Lang::get('app.position_sample')),
                                'class' => 'work_experience_value'
                            ]);
                    ?>
                </span>
            </div>

            <div class="work_description_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[description][]', mb_ucfirst(Lang::get('app.description'))); ?>
                </span>

                <span>
                    <?= Form::textarea('work[description][]',
                            $data['cvInfo']['workExperience'][$i]['description'],
                            [
                                'placeholder' => mb_ucfirst(Lang::get('app.description_sample')),
                                'class' => 'work_experience_value wysiwyg_area'
                            ]);
                    ?>
                </span>


            </div>
        </div>
    @endfor


    <div class="work_row">
        <?= Form::button(mb_ucfirst(Lang::get('app.add_work_experience')), ['id' => 'add_work']); ?>
    </div>


    <!-- template for new work experience -->
    <div class="work_experience_row" id="work_experience_row_template">
        <div class="company_name_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[company_name][]', mb_ucfirst(Lang::get('app.company_name'))); ?>
                </span>

                <span>
                    <?= Form::text('work[company_name][]',
                            '',
                            [
                                'placeholder' => mb_ucfirst(Lang::get('app.company_sample')),
                                'class' => 'work_experience_value'
                            ]);
                    ?>
                </span>

                <span>
                    <a class="delete_work_experience"
                       href=""><?= mb_ucfirst(Lang::get('app.remove_work_experience')); ?>
                    </a>
                </span>
        </div>

        <div class="work_start_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[date_start][]', mb_ucfirst(Lang::get('app.date_start'))); ?>
                </span>
                <span>
                    <?= Form::text('work[date_start][]',
                            '',
                            [
                                'placeholder' => mb_ucfirst(Lang::get('app.date_start_sample')),
                                'class' => 'work_experience_value work_experience_start_date_val'
                            ]);
                    ?>
                </span>
        </div>

        <div class="work_finish_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[date_finish][]', mb_ucfirst(Lang::get('app.date_finish'))); ?>
                </span>
                <span>
                    <?= Form::text('work[date_finish][]',
                            '',
                            [
                                'placeholder' => mb_ucfirst(Lang::get('app.date_finish_sample')),
                                'class' => 'work_experience_value work_experience_finish_date_val'
                            ]);
                    ?>
                </span>
        </div>

        <div class="work_position_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[position][]', mb_ucfirst(Lang::get('app.position'))); ?>
                </span>
                <span>
                    <?= Form::text('work[position][]',
                            '',
                            [
                                'placeholder' => mb_ucfirst(Lang::get('app.position_sample')),
                                'class' => 'work_experience_value'
                            ]);
                    ?>
                </span>
        </div>

        <div class="work_description_wrapper">
                <span class="work_experience_label">
                    <?= Form::label('work[description][]', mb_ucfirst(Lang::get('app.description'))); ?>
                </span>

                <span>
                    <?= Form::textarea('work[description][]',
                            '',
                            [
                                'placeholder' => mb_ucfirst(Lang::get('app.description_sample')),
                                'class' => 'work_experience_value'
                            ]);
                    ?>
                </span>
        </div>
    </div>

</div>