<hr />

<ul class="top_menu">
    <li>
        @foreach($data['menuItems'] as $item)
            @if ( $item['display'] === true) <a href="{{ $item['route'] }}">{{ $item['label'] }}</a> @endif
        @endforeach
    </li>
</ul>

<hr />