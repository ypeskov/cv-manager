<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">

    <title>CV Manager</title>

    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<body>
    <div id="page_wrapper">
        <div id="content_wrapper">
            @yield('content')
        </div>

        @section('footer')
        <div id="footer_wrapper">
            contact: yuriy.peskov@gmail.com
        </div>
        @show
    </div>
</body>

</html>