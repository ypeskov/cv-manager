<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">

    <title>CV: Yuriy Peskov</title>

    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<body>@yield('content')</body>

</html>