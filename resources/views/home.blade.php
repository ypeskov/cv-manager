@extends('layouts.html')

@section('content')
    @if ($data['isLoggedIn'])
        @include('top_menu')
    @endif

    <div>
        <div>Welcome to the site that will help you generate, store and manage Your CV in different formats.</div>
        <div>At this moment HTML and Pdf formats are supported. More formats are planned in future: Word, Image.</div>
        <div>We are also planning to add possibility to add possibility to send CV right from the site.</div>
    </div>
@stop
