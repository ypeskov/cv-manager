@extends('layouts.html')

@section('content')
<div>
    @include('user.error_messages')

    <?= Form::open([
            'name'      => 'changePasswordForm',
            'action'    => 'UserController@applyNewPassword',
            'files'     => false,
            'id'        => 'changePasswordForm'
    ]); ?>

    <div>
        <span class="register_label">
            <?= Form::label('old_password', mb_ucfirst(Lang::get('app.old_password'))); ?>:
        </span>
        <span>
            <?= Form::password('old_password',
                    ['placeholder' => Lang::get('app.old_password'), 'class' => 'register_value']); ?>
        </span>
    </div>

    @include('user.password_block')

    <div>
        <?= Form::submit(mb_ucfirst(Lang::get('app.apply_new_password'))); ?>
    </div>

    <?= Form::close(); ?>
</div>
@stop