<div id="register_errors">
    @if ( $errors->count() > 0 )
        <p>The following errors have occurred:</p>

        <ul>
            @foreach( $errors->all() as $message )
                <li class="error_msg">{{ $message }}</li>
            @endforeach
        </ul>
    @endif
</div>