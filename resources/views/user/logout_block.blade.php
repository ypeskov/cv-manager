<div id="logout_block">
    <div>
        <div><?= mb_ucfirst(Lang::get('app.welcome')); ?>,</div>
        <div>{{ $data['user']['email'] }}</div>
    </div>

    <div>
        <a href="{{ $data['uri']['logout'] }}" class="btn btn-primary"><?= mb_ucfirst(Lang::get('app.logout')); ?></a>
    </div>

    <div>
        <a href="{{ $data['uri']['changePasswordForm'] }}"><?= mb_ucfirst(Lang::get('app.change_password')); ?></a>
    </div>
</div>