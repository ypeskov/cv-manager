<div id="login_block">
    <?= Form::open([
            'name'      => 'userForm',
            'action'    => 'UserController@login',
            'files'     => false,
            'id'        => 'user_login_form',
    ]); ?>

    <div>
        <?= Form::text('email',
                '',
                [
                    'id'            => 'user_email',
                    'placeholder'   => mb_ucfirst(Lang::get('app.email')),
                    'class'         => 'login_value'
                ]); ?>

        <?= Form::password('password',
                [
                    'id'            => 'user_password',
                    'placeholder'   => mb_ucfirst(Lang::get('app.password')),
                    'class'         => 'login_value'
                ]); ?>
    </div>

    <div>
        <?= Form::submit(mb_ucfirst(Lang::get('app.login')),
                        ['id' => 'login_button',
                         'class' => 'btn btn-primary']); ?>
    </div>

    <?= Form::close(); ?>

    <div>
        <a href="{{ $data['uri']['registerForm'] }}"><?= mb_ucfirst(Lang::get('app.dont_have_account?')); ?></a>
    </div>
</div>