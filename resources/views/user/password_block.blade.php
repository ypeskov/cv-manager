<div>
        <span class="register_label">
            <?= Form::label('user_password', mb_ucfirst(Lang::get('app.password'))); ?>:
        </span>
        <span>
            <?= Form::password('user_password',
                    ['placeholder' => Lang::get('app.password'), 'class' => 'register_value']); ?>
        </span>
</div>

<div>
        <span class="register_label">
            <?= Form::label('user_password_confirmation', mb_ucfirst(Lang::get('app.password_confirmation'))); ?>:
        </span>
        <span>
            <?= Form::password('user_password_confirmation',
                    ['placeholder' => Lang::get('app.password_confirmation'), 'class' => 'register_value']); ?>
        </span>
</div>