@extends('layouts.html')

@section('content')
    @include('user.error_messages')

    <?= Form::open([
            'name'      => 'userForm',
            'action'    => 'UserController@registeruser',
            'files'     => false,
            'id'        => 'newUserForm'
    ]); ?>

    <div>
        <span class="register_label">
            <?= Form::label('user_email', mb_ucfirst(Lang::get('app.email'))); ?>:
        </span>
        <span>
            <?= Form::text('user_email',
                    '',
                    ['placeholder' => Lang::get('app.email_sample'), 'class' => 'register_value']); ?>
        </span>
    </div>

    @include('user.password_block')

    <div>
        <?= Form::submit(Lang::get('app.register')); ?>
    </div>

    <?= Form::close(); ?>
</div>
@stop