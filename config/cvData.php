<?php

return [
    //engine for storing CV data
    'engine'    => 'eloquent',

    //db name
    'dbName'    => 'cv_manager',

    //db host
    'dbHost'    => 'localhost',

    //db user
    'dbUser'    => 'cv_manager',

    //db password
    'dbPassword'=> 'cv_manager',
];