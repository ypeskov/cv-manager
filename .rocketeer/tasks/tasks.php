<?php

use Rocketeer\Facades\Rocketeer;

Rocketeer::add('tasks\SettingsBackup');
Rocketeer::add('tasks\SettingsRestore');
Rocketeer::add('tasks\PrepareClientApp');