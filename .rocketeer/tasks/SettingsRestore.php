<?php

namespace tasks;


class SettingsRestore extends \Rocketeer\Abstracts\AbstractTask
{
    protected $description = "Restores environment settings after deployment";

    public function execute()
    {
        $fullEnvFileName = '/tmp/.env';
        $currentPath = $this->releasesManager->getCurrentReleasePath() . '/';

        if ( $this->fileExists($fullEnvFileName) ) {
            $this->runForCurrentRelease('cp ' . $fullEnvFileName . ' ' . $currentPath);

            $this->runForCurrentRelease('composer dump-autoload');

        } else {
            throw new \Exception($fullEnvFileName . ' not found');
        }
    }
}