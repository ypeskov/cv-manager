<?php

namespace tasks;

class PrepareClientApp extends \Rocketeer\Abstracts\AbstractTask
{
    protected $description = "Move client app to public folder";

    public function execute()
    {
        $currentPath    = $this->releasesManager->getCurrentReleasePath();
        $sourceDir      = 'client_app/bin';
        $bootstrapDir   = 'client_app/vendor/bootstrap';
        $webServerDir   = 'spa';
        $clientAppDir   = 'client_app';

        $this->runForCurrentRelease("mkdir $webServerDir/$clientAppDir");
        $this->runForCurrentRelease("cp -ar $sourceDir/* $currentPath/$webServerDir/$clientAppDir");

        $this->runForCurrentRelease("cp -ar $bootstrapDir/fonts $currentPath/$webServerDir/$clientAppDir");

        $this->runForCurrentRelease("chmod g+w $currentPath/ -R");
    }
}