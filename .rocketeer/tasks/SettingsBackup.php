<?php

namespace tasks;

class SettingsBackup extends \Rocketeer\Abstracts\AbstractTask
{
    protected $description  = "Backups environment settings before deployment";

    public function execute()
    {
        $currentPath = $this->releasesManager->getCurrentReleasePath() . '/';
        $fileName = '.env';
        $tmpDir = '/tmp/';

        if ( $this->fileExists($currentPath . $fileName) ) {
            $result = $this->runForCurrentRelease('cp ' . $currentPath . $fileName . ' ' . $tmpDir);

            if ( (!$this->fileExists('/tmp/.env')) or ($result !== "") ) {
                throw new \Exception($currentPath . $fileName . ' wasn\'t copied');
            }
        } else {
            throw new \Exception($currentPath . $fileName . ' is not found');
        }

    }
}