<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherInfoTable extends Migration
{
    const TABLE_NAME = 'other_info';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function($table) {
            $table->increments('id');

            $table->string('info_name', 50);
            $table->string('info_value', 100);

            $table->integer('personal_info_id')->unsigned();
            $table->foreign('personal_info_id')
                ->references('id')->on(CreatePersonalInfoTable::TABLE_NAME)
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }

}
