<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkExperienceTable extends Migration
{
    const TABLE_NAME = 'work_experience';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function($table) {
            $table->increments('id');

            $table->string('company_name', 150);
            $table->date('date_start');
            $table->date('date_finish');
            $table->string('position', 100);
            $table->text('description');

            $table->integer('personal_info_id')->unsigned();
            $table->foreign('personal_info_id')
                ->references('id')->on(CreatePersonalInfoTable::TABLE_NAME)
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }

}
