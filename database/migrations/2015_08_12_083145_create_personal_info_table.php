<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalInfoTable extends Migration
{
    const TABLE_NAME = 'personal_info';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function($table) {
            $table->increments('id');

            $table->string('cv_name', 30);
            $table->string('first_name', 30);
            $table->string('last_name', 30);
            $table->date('birth_date');
            $table->string('email', 50);
            $table->string('skype', 30);
            $table->string('contact_phone', 15);

            $table->integer('locale_id')->unsigned();
            $table->foreign('locale_id')
                ->references('id')->on(CreateLocalesTable::TABLE_NAME)
                ->onDelete('cascade')->onUpdate('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on(CreateUsersTable::TABLE_NAME)
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }

}
