<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOtherInfoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = CreateOtherInfoTable::TABLE_NAME;
        $columnName = 'info_value';

        DB::statement('alter table `' . $tableName . '` modify `' . $columnName . '` text');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableName = CreateOtherInfoTable::TABLE_NAME;
        $columnName = 'info_value';

        DB::statement('alter table `' . $tableName . '` modify `' . $columnName . '` varchar(100)');
    }

}
