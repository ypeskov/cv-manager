<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsTable extends Migration
{
    const TABLE_NAME = 'skills';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function($table) {
            $table->increments('id');

            $table->string('group_name', 30);
            $table->string('group_content', 150);

            $table->integer('personal_info_id')->unsigned();
            $table->foreign('personal_info_id')
                ->references('id')->on(CreatePersonalInfoTable::TABLE_NAME)
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }

}
