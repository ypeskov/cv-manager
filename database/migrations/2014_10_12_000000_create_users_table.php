<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    const NAME_MAX_LENGTH   = 100;
    const EMAIL_MAX_LENGTH      = 100;
    const PASSWORD_HASH_MAX_LENGTH=64;
    const TABLE_NAME    = 'users';


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('name', self::NAME_MAX_LENGTH);
            $table->string('email', self::EMAIL_MAX_LENGTH)->unique();
            $table->string('password', self::PASSWORD_HASH_MAX_LENGTH);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
