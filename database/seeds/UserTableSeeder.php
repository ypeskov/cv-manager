<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    const TABLE_NAME   = 'users';

    public function run()
    {
        DB::table(self::TABLE_NAME)->delete();

        User::create([
                'name'  => 'Yura Peskov',
                'email' => 'yuriy.peskov@gmail.com',
                'password'  => Hash::make('111'),
        ]);

        User::create([
            'name'  => 'Yura Peskov',
            'email' => 'ypeskov@pisem.net',
            'password'  => Hash::make('123'),
        ]);
    }
}