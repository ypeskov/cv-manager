<?php

use Illuminate\Database\Seeder;

class LocaleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(CreateLocalesTable::TABLE_NAME)->delete();

        Locale::create([
            'locale_name'           => 'en_US',
            'language_short_name'   => 'English',
            'language_long_name'    => 'English (US)'
        ]);

        Locale::create([
            'locale_name'           => 'en',
            'language_short_name'   => 'English',
            'language_long_name'    => 'English'
        ]);

        Locale::create([
            'locale_name'           => 'uk_UA',
            'language_short_name'   => 'Українська',
            'language_long_name'    => 'Українська (Україна)'
        ]);

        Locale::create([
            'locale_name'           => 'ru_UA',
            'language_short_name'   => 'Русский',
            'language_long_name'    => 'Русский (Україна)'
        ]);

        Locale::create([
            'locale_name'           => 'ru_RU',
            'language_short_name'   => 'Русский',
            'language_long_name'    => 'Русский (Россия)'
        ]);
    }
}
