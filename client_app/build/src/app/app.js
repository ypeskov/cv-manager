angular.module( 'cvManager', [
    'templates-app',
    'templates-common',

    'cvManager.ajax-loader',
    'cvManager.header',
    'cvManager.home',
    'cvManager.bikeLocale',
    'cvManager.user',
    'cvManager.dashboard',
    'cvManager.auth',
    'cvManager.cv-form',
    'cvManager.token',

    'ui.router',
    'ui.bootstrap.tpls',
    'ui.bootstrap.datepicker'
])

.config( ['$stateProvider', '$urlRouterProvider', '$httpProvider',
function( $stateProvider, $urlRouterProvider, $httpProvider ) {
    $urlRouterProvider.otherwise( '/home' );

    $httpProvider.interceptors.push('cvHttpInterceptor');
}])

.run(['$rootScope', '$state', 'AUTH_EVENTS', 'AuthFactory', 'Session', 'USER_REGISTER_CONST',
        'HOME_CONST', 'DASHBOARD_CONST',
function($rootScope, $state, AUTH_EVENTS, AuthFactory, Session, USER_REGISTER_CONST,
        HOME_CONST, DASHBOARD_CONST) {
    Session.init();

    $rootScope.$on('$stateChangeStart',
        function(event, next, nextParams, fromState, fromParams, error) {

        //if a user is not authenticated check that only home page is accessible
        if ( !AuthFactory.isAuthenticated() ) {
            if ( (next.name !== HOME_CONST.homeState) &&
                (next.name !== USER_REGISTER_CONST.stateName) ) {
                console.log('not authenticated!');
                event.preventDefault();
                $state.go(HOME_CONST.homeState);
            }

            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
        } else {
            if ( next.name === HOME_CONST.homeState ) {
                $state.go(DASHBOARD_CONST.stateName);
            }
        }
    });
}])

.controller( 'AppCtrl', ['$scope', 'bikeLocale', 'AuthFactory', 'UserFactory',
function( $scope, bikeLocale, AuthFactory, UserFactory ) {
    var _self = this;

    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
        if ( angular.isDefined( toState.data.pageTitle ) ) {
          $scope.pageTitle = toState.data.pageTitle + ' | cvManager' ;
        }
    });

    _self.lang = bikeLocale.getStrings();
    _self.getStrUp = bikeLocale.getStrUp;

    _self.user = UserFactory.getUser();

    _self.logout = function() {
        return AuthFactory.logout();
    };

    return $scope.AppCtrl = _self;
}])


;

