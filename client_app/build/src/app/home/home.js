angular.module('cvManager.home', [
    'ui.router',

    'cvManager.bikeLocale',
    'cvManager.auth',
    'cvManager.token'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(['$stateProvider',
function config($stateProvider) {
    $stateProvider
        .state('home', {
            url: '/home',

            views: {
                "main": {
                    controller: 'HomeCtrl',
                    templateUrl: 'home/home.tpl.html'
                }
            },

            data: {pageTitle: 'Home'}
        });
}])

/**
 * And of course we define a controller for our route.
 */
.controller('HomeCtrl', ['$scope',
function ($scope) {
    "use strict";
    var _self = this;

    return $scope.HomeCtrl = _self;
}])

.controller('HomeMainCtrl', ['$scope',
function($scope) {
    var _self = this;

    return $scope.HomeMainCtrl = _self;
}])

.constant('HOME_CONST',
{
    homeState:      'home'
})

;

