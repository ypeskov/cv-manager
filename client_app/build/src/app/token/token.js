(function(){
    'use strict';

    function TokenService($http)
    {
        return {
            get: get
        };

        ////
        function get() {
            return $http.get('/get-token').then(
                success,
                fail
            );
        }

        function success(response) {
            return response;
        }

        function fail(response) {
            return response;
        }
    }
    TokenService.$inject = ["$http"];

    angular.module('cvManager.token', [])

    .factory('TokenService', TokenService);

})();