angular.module('templates-app', ['ajax-loader/ajax-loader.tpl.html', 'calendar/calendar.tpl.html', 'cv-form/new-cv.tpl.html', 'cv-manager/cv-manager.tpl.html', 'dashboard/dashboard.tpl.html', 'header/header.tpl.html', 'home/home.tpl.html', 'home/home_main.tpl.html', 'user/user-register.tpl.html', 'user/user_login.tpl.html', 'user/user_logout.tpl.html']);

angular.module("ajax-loader/ajax-loader.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("ajax-loader/ajax-loader.tpl.html",
    "<div class=\"ajax_loader\"></div>");
}]);

angular.module("calendar/calendar.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("calendar/calendar.tpl.html",
    "<div class=\"cv-calendar-container\">\n" +
    "    <span></span>\n" +
    "\n" +
    "    <div class=\"cv-calendar-popup-wrapper cv-calendar-hidden\">\n" +
    "        <div class=\"cv-calendar-month\">\n" +
    "            <span></span>\n" +
    "            <span>{{ year }}</span>\n" +
    "            <span></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"cv-calendar-year\">\n" +
    "            <span></span>\n" +
    "            <span>{{ month }}</span>\n" +
    "            <span></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"cv-calendar-dates\">\n" +
    "            <p ng-repeat=\"date in monthDates\">\n" +
    "                <a href=\"\" ng-click=\"setD(date)\">{{ date.val }} - {{ date.current }}</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("cv-form/new-cv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("cv-form/new-cv.tpl.html",
    "<div ng-include=\"'header/header.tpl.html'\" ng-controller=\"HeaderCtrl\"></div>\n" +
    "\n" +
    "<div class=\"row\">\n" +
    "    <div id=\"top_menu\" class=\"top_menu\">\n" +
    "        <ul ng-repeat=\"item in newCvCtrl.menuItems track by $index\">\n" +
    "            <li><a href=\"\" ng-click=\"item.action()\">{{ ::AppCtrl.getStrUp(item.label) }}</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<form name=\"new_cv\" ng-submit=\"newCvCtrl.addCV()\" novalidate>\n" +
    "    <div id=\"cv_personal_info\" class=\"cv_section\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <div class=\"cv_section_subtitle\">{{ ::AppCtrl.getStrUp('personal_information') }}</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"locale_id\">{{ ::AppCtrl.getStrUp('language') }}</label>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <select id=\"locale_id\"\n" +
    "                        name=\"locale_id\"\n" +
    "                        class=\"cv_personal_info_field\"\n" +
    "                        ng-model=\"newCvCtrl.cv.personal_info.locale_id\"\n" +
    "                        ng-options=\"locale.id as locale.language_long_name for locale in newCvCtrl.locales\"\n" +
    "                        required >\n" +
    "                    <option value=\"\">{{ ::AppCtrl.getStrUp('select_language') }}</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-3\">\n" +
    "                <span class=\"error_label\"\n" +
    "                      ng-show=\"new_cv.locale_id.$invalid && !new_cv.locale_id.$pristine\">\n" +
    "                    {{ ::AppCtrl.getStrUp('language_required') }}\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"cv_name\">{{ ::AppCtrl.getStrUp('cv_name') }}</label>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <input id=\"cv_name\"\n" +
    "                       name=\"cv_name\"\n" +
    "                       class=\"cv_personal_info_field\"\n" +
    "                       type=\"text\"\n" +
    "                       ng-model=\"newCvCtrl.cv.personal_info.cv_name\" required />\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-3\">\n" +
    "                <span class=\"error_label\"\n" +
    "                      ng-show=\"new_cv.cv_name.$invalid && !new_cv.cv_name.$pristine\">\n" +
    "                    {{ ::AppCtrl.getStrUp('cv_name_required') }}\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"first_name\">{{ ::AppCtrl.getStrUp('first_name') }}</label>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <input id=\"first_name\"\n" +
    "                       name=\"first_name\"\n" +
    "                       class=\"cv_personal_info_field\"\n" +
    "                       type=\"text\"\n" +
    "                       ng-model=\"newCvCtrl.cv.personal_info.first_name\" required />\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-3\">\n" +
    "                <span class=\"error_label\" ng-show=\"new_cv.first_name.$invalid && !new_cv.first_name.$pristine\">\n" +
    "                    {{ ::AppCtrl.getStrUp('first_name_required') }}\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"last_name\">{{ ::AppCtrl.getStrUp('last_name') }}</label>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <input id=\"last_name\"\n" +
    "                       class=\"cv_personal_info_field\"\n" +
    "                       type=\"text\"\n" +
    "                       ng-model=\"newCvCtrl.cv.personal_info.last_name\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"birth_date\">{{ ::AppCtrl.getStrUp('birth_date') }}</label>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-2\">\n" +
    "\n" +
    "                <p class=\"input-group\">\n" +
    "                    <input id=\"birth_date\"\n" +
    "                           type=\"text\"\n" +
    "                           class=\"form-control\"\n" +
    "                           datepickerLocaldate\n" +
    "                           datepicker-popup=\"{{ newCvCtrl.dateFormat }}\"\n" +
    "                           datepicker-options=\"dateOptions\"\n" +
    "                           ng-model=\"newCvCtrl.cv.personal_info.birth_date\"\n" +
    "                           is-open=\"newCvCtrl.birthDateOpened\"\n" +
    "                           show-button-bar=\"false\"\n" +
    "                    />\n" +
    "                    <span class=\"input-group-btn\">\n" +
    "                        <button type=\"button\"\n" +
    "                                class=\"btn btn-default\"\n" +
    "                                ng-click=\"newCvCtrl.open($event)\">\n" +
    "                            <i class=\"glyphicon glyphicon-calendar\"></i>\n" +
    "                        </button>\n" +
    "                    </span>\n" +
    "                </p>\n" +
    "                -- {{ newCvCtrl.cv.personal_info.birth_date }} --\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"email\">{{ ::AppCtrl.getStrUp('email') }}</label>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <input id=\"email\"\n" +
    "                       name=\"email\"\n" +
    "                       class=\"cv_personal_info_field\"\n" +
    "                       type=\"text\"\n" +
    "                       ng-model=\"newCvCtrl.cv.personal_info.email\" required />\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-3\">\n" +
    "                <span class=\"error_label\" ng-show=\"new_cv.email.$invalid && new_cv.email.$dirty\">\n" +
    "                    {{ ::AppCtrl.getStrUp('email_required') }}\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"skype\">{{ ::AppCtrl.getStrUp('skype') }}</label>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <input id=\"skype\"\n" +
    "                       class=\"cv_personal_info_field\"\n" +
    "                       type=\"text\"\n" +
    "                       ng-model=\"newCvCtrl.cv.personal_info.skype\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"contact_phone\">{{ ::AppCtrl.getStrUp('contact_phone') }}</label>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <input id=\"contact_phone\"\n" +
    "                       class=\"cv_personal_info_field\"\n" +
    "                       type=\"text\"\n" +
    "                       ng-model=\"newCvCtrl.cv.personal_info.contact_phone\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div id=\"cv_skills\" class=\"cv_section\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <div class=\"cv_section_subtitle\">{{ ::AppCtrl.getStrUp('skills') }}</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\"\n" +
    "             ng-repeat=\"skillItem in newCvCtrl.cv.skills track by $index\">\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <label for=\"cv_skill_name_{{ $index }}\">{{ ::AppCtrl.getStrUp('skill_group') }}</label>\n" +
    "                <input id=\"cv_skill_name_{{ $index }}\"\n" +
    "                       class=\"cv_skill_field\"\n" +
    "                       ng-model=\"newCvCtrl.cv.skills[$index].group_name\"\n" +
    "                       type=\"text\" />\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-7\">\n" +
    "                <label for=\"cv_skill_val_{{ $index }}\">{{ ::AppCtrl.getStrUp('skill_value') }}</label>\n" +
    "                <input id=\"cv_skill_val_{{ $index }}\"\n" +
    "                       class=\"cv_skill_field\"\n" +
    "                       ng-model=\"newCvCtrl.cv.skills[$index].group_content\"\n" +
    "                       type=\"text\" />\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-1\">\n" +
    "                <button class=\"cv_skill_remove_button\"\n" +
    "                        ng-click=\"newCvCtrl.removeSkill($index)\">{{ ::AppCtrl.getStrUp('remove') }}</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <button id=\"cv_skill_more_button\"\n" +
    "                        ng-click=\"newCvCtrl.addSkill()\">{{ ::AppCtrl.getStrUp('more_skills') }}</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div id=\"cv_education\" class=\"cv_section\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <div class=\"cv_section_subtitle\">{{ ::AppCtrl.getStrUp('education') }}</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\" ng-repeat=\"skillItem in newCvCtrl.cv.educations track by $index\">\n" +
    "            <div class=\"col-xs-5\">\n" +
    "                <label for=\"education_inst_{{ $index }}\">{{ AppCtrl.getStrUp('institution') }}</label>\n" +
    "                <input id=\"education_inst_{{ $index }}\"\n" +
    "                       class=\"cv_edu_institution_value\"\n" +
    "                       ng-model=\"newCvCtrl.cv.educations[$index].educational_inst\"\n" +
    "                       type=\"text\"\n" +
    "                />\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"education_start_{{ $index }}\">{{ AppCtrl.getStrUp('start_date') }}</label>\n" +
    "\n" +
    "                <p class=\"input-group\">\n" +
    "                    <input id=\"education_start_{{ $index }}\"\n" +
    "                           type=\"text\"\n" +
    "                           class=\"form-control\"\n" +
    "                           datepicker-popup=\"{{ newCvCtrl.dateFormat }}\"\n" +
    "                           datepicker-options=\"dateOptions\"\n" +
    "                           ng-model=\"newCvCtrl.cv.educations[$index].date_start\"\n" +
    "                           is-open=\"newCvCtrl.cv.educations[$index].displayStartDate\"\n" +
    "                           show-button-bar=\"false\"\n" +
    "                            />\n" +
    "                    <span class=\"input-group-btn\">\n" +
    "                        <button type=\"button\"\n" +
    "                                class=\"btn btn-default\"\n" +
    "                                ng-click=\"newCvCtrl.openEducationCal($index, 0)\">\n" +
    "                            <i class=\"glyphicon glyphicon-calendar\"></i>\n" +
    "                        </button>\n" +
    "                    </span>\n" +
    "                </p>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"education_end_{{ $index }}\">{{ AppCtrl.getStrUp('end_date') }}</label>\n" +
    "\n" +
    "                <p class=\"input-group\">\n" +
    "                    <input id=\"education_end_{{ $index }}\"\n" +
    "                           type=\"text\"\n" +
    "                           class=\"form-control\"\n" +
    "                           datepicker-popup=\"{{ newCvCtrl.dateFormat }}\"\n" +
    "                           datepicker-options=\"dateOptions\"\n" +
    "                           ng-model=\"newCvCtrl.cv.educations[$index].date_finish\"\n" +
    "                           is-open=\"newCvCtrl.cv.educations[$index].displayFinishDate\"\n" +
    "                           show-button-bar=\"false\"\n" +
    "                            />\n" +
    "                    <span class=\"input-group-btn\">\n" +
    "                        <button type=\"button\"\n" +
    "                                class=\"btn btn-default\"\n" +
    "                                ng-click=\"newCvCtrl.openEducationCal($index, 1)\">\n" +
    "                            <i class=\"glyphicon glyphicon-calendar\"></i>\n" +
    "                        </button>\n" +
    "                    </span>\n" +
    "                </p>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <label for=\"education_speciality_{{ $index }}\">{{ AppCtrl.getStrUp('speciality') }}</label>\n" +
    "                <input id=\"education_speciality_{{ $index }}\"\n" +
    "                       class=\"cv_edu_speciality_value\"\n" +
    "                       ng-model=\"newCvCtrl.cv.educations[$index].specialty\"\n" +
    "                       type=\"text\"\n" +
    "                />\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-xs-1\">\n" +
    "                <button ng-click=\"newCvCtrl.removeEducation($index)\">{{ ::AppCtrl.getStrUp('remove') }}</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <button ng-click=\"newCvCtrl.addEducation()\">{{ ::AppCtrl.getStrUp('add_education') }}</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div id=\"cv_work\" class=\"cv_section\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-4\">\n" +
    "                <div class=\"cv_section_subtitle\">{{ ::AppCtrl.getStrUp('work experience') }}</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"work in newCvCtrl.cv.workExperiences track by $index\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <label for=\"cv_company_name_{{ $index }}\">{{ AppCtrl.getStrUp('company_name') }}</label>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-7\">\n" +
    "                    <input id=\"cv_company_name_{{ $index }}\"\n" +
    "                           class=\"cv_work_company_name\"\n" +
    "                           ng-model=\"newCvCtrl.cv.workExperiences[$index].company_name\"\n" +
    "                           type=\"text\"\n" +
    "                            />\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-3\">\n" +
    "                    <button ng-click=\"newCvCtrl.removeWork()\">{{ AppCtrl.getStrUp('remove_work_experience') }}</button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <label for=\"cv_start_date_{{ $index }}\">{{ AppCtrl.getStrUp('start_date') }}</label>\n" +
    "                </div>\n" +
    "{{ newCvCtrl.dateOptions }}\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <input id=\"cv_start_date_{{ $index }}\"\n" +
    "                           type=\"text\"\n" +
    "                           class=\"form-control\"\n" +
    "                           datepicker-options=\"newCvCtrl.dateOptions\"\n" +
    "                           datepicker-popup=\"yyyy-MM-dd\"\n" +
    "                           ng-model=\"newCvCtrl.cv.workExperiences[$index].date_start\"\n" +
    "                           show-weeks=\"false\"\n" +
    "                           show-button-bar=\"false\"\n" +
    "                            />\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <label for=\"cv_end_date_{{ $index }}\">{{ AppCtrl.getStrUp('end_date') }}</label>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-6\">\n" +
    "                    <input id=\"cv_end_date_{{ $index }}\"\n" +
    "                           class=\"cv_work_end_date\"\n" +
    "                           ng-model=\"newCvCtrl.cv.workExperiences[$index].date_finish\"\n" +
    "                           type=\"text\"\n" +
    "                            />\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <label for=\"cv_position_{{ $index }}\">{{ AppCtrl.getStrUp('position') }}</label>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-6\">\n" +
    "                    <input id=\"cv_position_{{ $index }}\"\n" +
    "                           class=\"cv_work_position\"\n" +
    "                           ng-model=\"newCvCtrl.cv.workExperiences[$index].position\"\n" +
    "                           type=\"text\"\n" +
    "                            />\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <label for=\"cv_position_{{ $index }}\">{{ AppCtrl.getStrUp('position') }}</label>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-8\">\n" +
    "                    <textarea id=\"cv_description_{{ $index }}\"\n" +
    "                           class=\"cv_work_description\"\n" +
    "                           type=\"text\"\n" +
    "                           ng-model=\"newCvCtrl.cv.workExperiences[$index].description\" ></textarea>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-2\">\n" +
    "                <button ng-click=\"newCvCtrl.addWork()\">{{ ::AppCtrl.getStrUp('add_work') }}</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-2\">\n" +
    "            <button ng-click=\"newCvCtrl.saveCv()\">{{ ::AppCtrl.getStrUp('save_cv') }}</button>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-3\">\n" +
    "            <span class=\"error_label\"\n" +
    "                  ng-show=\"new_cv.$invalid && new_cv.$submitted\">{{ ::AppCtrl.getStrUp('please_check_errors') }}</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</form>");
}]);

angular.module("cv-manager/cv-manager.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("cv-manager/cv-manager.tpl.html",
    "<div ng-include=\"'header/header.tpl.html'\" ng-controller=\"HeaderCtrl\"></div>\n" +
    "\n" +
    "<div class=\"row\">\n" +
    "    <div id=\"top_menu\" class=\"top_menu\">\n" +
    "        <ul ng-repeat=\"item in CvManagerCtrl.menuItems track by $index\">\n" +
    "            <li><a href=\"\" ng-click=\"item.action()\">{{ ::AppCtrl.getStrUp(item.label) }}</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row\">\n" +
    "    <div class=\"col-xs-1\"><span class=\"cv_table_title\">{{ ::AppCtrl.getStrUp('cv_id') }}</span></div>\n" +
    "\n" +
    "    <div class=\"col-xs-7\"><span class=\"cv_table_title\">{{ ::AppCtrl.getStrUp('cv_information') }}</span></div>\n" +
    "\n" +
    "    <div class=\"col-xs-4\"><span class=\"cv_table_title\">{{ ::AppCtrl.getStrUp('cv_control') }}</span></div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"row cv_item\" ng-repeat=\"cvItem in CvManagerCtrl.cvList track by $index\">\n" +
    "    <div class=\"col-xs-1\">\n" +
    "        {{ ::cvItem.id }}\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"col-xs-7\">\n" +
    "        {{ ::cvItem.display }}\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"col-xs-4\">\n" +
    "        <a class=\"cv_edit_button btn btn-primary\"\n" +
    "           ng-click=\"CvManagerCtrl.editCv(cvItem)\">{{ ::AppCtrl.getStrUp('edit') }}</a>\n" +
    "\n" +
    "        <a class=\"cv_edit_button btn btn-primary\"\n" +
    "                ng-href=\"/cv/view/{{ cvItem.id }}/html\"\n" +
    "                target=\"_blank\">{{ ::AppCtrl.getStrUp('view_html') }}</a>\n" +
    "\n" +
    "        <a class=\"cv_edit_button btn btn-primary\"\n" +
    "                ng-href=\"/cv/view/{{ cvItem.id }}/pdf\"\n" +
    "                target=\"_blank\">{{ ::AppCtrl.getStrUp('view_pdf') }}</a>\n" +
    "\n" +
    "        <a class=\"cv_edit_button btn btn-primary\"\n" +
    "           ng-click=\"CvManagerCtrl.deleteCv(cvItem)\">{{ ::AppCtrl.getStrUp('delete') }}</a>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("dashboard/dashboard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("dashboard/dashboard.tpl.html",
    "<div ng-include=\"'header/header.tpl.html'\" ng-controller=\"HeaderCtrl\"></div>\n" +
    "\n" +
    "<div class=\"row\">\n" +
    "    <div id=\"top_menu\" class=\"top_menu\">\n" +
    "        <ul>\n" +
    "            <li ng-repeat=\"item in DashboardCtrl.menuItems track by $index\">\n" +
    "                <a href=\"\" ng-click=\"item.action()\">{{ ::AppCtrl.getStrUp(item.label) }}</a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "\n" +
    "    <div id=\"main_content\" class=\"col-xs-12\" ng-include='\"home/home_main.tpl.html\"'></div>\n" +
    "</div>");
}]);

angular.module("header/header.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("header/header.tpl.html",
    "<div class=\"row\">\n" +
    "    <div id=\"top_banner_container\" class=\"col-xs-8\">\n" +
    "        Banner blYa\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"col-xs-4\" ng-switch =\"HeaderCtrl.loggedIn\" >\n" +
    "        <span ng-switch-when=\"false\">\n" +
    "            <div ng-include=\"'user/user_login.tpl.html'\"></div>\n" +
    "        </span>\n" +
    "\n" +
    "        <span ng-switch-when=\"true\">\n" +
    "            <div ng-include=\"'user/user_logout.tpl.html'\"></div>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "\n" +
    "</div>");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<div ng-include=\"'header/header.tpl.html'\" ng-controller=\"HeaderCtrl\"></div>\n" +
    "\n" +
    "<div class=\"row\">\n" +
    "    <div id=\"main_content\" class=\"col-xs-12\"\n" +
    "         ng-include='\"home/home_main.tpl.html\"'></div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("home/home_main.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home_main.tpl.html",
    "<div ng-controller=\"HomeMainCtrl\">\n" +
    "    <div>{{ ::AppCtrl.lang['Welcome to the site that will help you generate, store and manage Your CV in different formats.'] }}</div>\n" +
    "    <div>{{ ::AppCtrl.lang['At this moment HTML and Pdf formats are supported. More formats are planned in future: Word, Image.'] }}</div>\n" +
    "    <div>{{ ::AppCtrl.lang['We are also planning to add possibility to add possibility to send CV right from the site.'] }}</div>\n" +
    "</div>");
}]);

angular.module("user/user-register.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("user/user-register.tpl.html",
    "<div ng-include=\"'header/header.tpl.html'\" ng-controller=\"HeaderCtrl\"></div>\n" +
    "\n" +
    "<div class=\"row\">\n" +
    "    <br />\n" +
    "    <div class=\"col-xs-12\">\n" +
    "        <form name=\"user_register_form\"\n" +
    "              ng-submit=\"UserRegisterCtrl.register(newUser)\"\n" +
    "              novalidate=\"true\"  >\n" +
    "\n" +
    "            <input name=\"_token\" type=\"hidden\" value=\"{{ ::UserRegisterCtrl.token }}\" />\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <label for=\"user_register_email\">{{ ::AppCtrl.getStrUp('email') }}</label>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-3\">\n" +
    "                    <input id=\"user_register_email\"\n" +
    "                           type=\"text\"\n" +
    "                           name=\"email\"\n" +
    "                           ng-model=\"newUser.email\"\n" +
    "                           ng-change=\"UserRegisterCtrl.hideEmailErrors()\"\n" +
    "                           required />\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-3\">\n" +
    "                    <span class=\"error_label\"\n" +
    "                          ng-show=\"user_register_form.email.$invalid\n" +
    "                                    && user_register_form.email.$dirty\">\n" +
    "                        {{ ::AppCtrl.getStrUp('email_required') }}\n" +
    "                    </span>\n" +
    "\n" +
    "                    <span class=\"error_label\"\n" +
    "                          ng-show=\"UserRegisterCtrl.errors.emailErrorsShow\">\n" +
    "                        {{ UserRegisterCtrl.errors.emailErrors }}\n" +
    "                    </span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <label for=\"user_register_password\">{{ ::AppCtrl.getStrUp('password') }}</label>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-3\">\n" +
    "                    <input id=\"user_register_password\"\n" +
    "                           type=\"password\"\n" +
    "                           name=\"password\"\n" +
    "                           ng-model=\"newUser.password\"\n" +
    "                           ng-change=\"UserRegisterCtrl.hidePasswordErrors()\"\n" +
    "                           required />\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-3\">\n" +
    "                    <span class=\"error_label\"\n" +
    "                          ng-show=\"user_register_form.password.$invalid\n" +
    "                                    && user_register_form.password.$dirty\">\n" +
    "                        {{ ::AppCtrl.getStrUp('password_required') }}\n" +
    "                    </span>\n" +
    "\n" +
    "                    <span class=\"error_label\"\n" +
    "                          ng-show=\"UserRegisterCtrl.errors.passwordErrorsShow\">\n" +
    "                        {{ UserRegisterCtrl.errors.passwordErrors }}\n" +
    "                    </span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-2\">\n" +
    "                    <label for=\"user_register_password_confirm\">{{ ::AppCtrl.getStrUp('password_confirm') }}</label>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-3\">\n" +
    "                    <input id=\"user_register_password_confirm\"\n" +
    "                           type=\"password\"\n" +
    "                           name=\"password_confirmation\"\n" +
    "                           ng-model=\"newUser.password_confirmation\"\n" +
    "                           ng-change=\"UserRegisterCtrl.hidePasswordErrors()\"\n" +
    "                           required />\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-3\">\n" +
    "                    <span class=\"error_label\"\n" +
    "                          ng-show=\"user_register_form.password_confirmation.$invalid\n" +
    "                                    && user_register_form.password_confirmation.$dirty\">\n" +
    "                        {{ ::AppCtrl.getStrUp('password_confirmation_required') }}\n" +
    "                    </span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <button id=\"user_register_button\">{{ ::AppCtrl.getStrUp('register') }}</button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("user/user_login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("user/user_login.tpl.html",
    "<form name=\"user_login\" ng-controller=\"UserCtrl\" ng-submit=\"UserCtrl.login(credentials)\">\n" +
    "\n" +
    "    <input type=\"hidden\"\n" +
    "           name=\"_token\"\n" +
    "           ng-model=\"credentials._token\"\n" +
    "           value=\"{{ _token }}\" />\n" +
    "\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-4\">\n" +
    "            <label for=\"user_login\">{{ ::AppCtrl.getStrUp('email') }}</label>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-5\">\n" +
    "            <input id=\"user_login\"\n" +
    "                   name=\"username\"\n" +
    "                   type=\"text\"\n" +
    "                   ng-model=\"credentials.username\"\n" +
    "                   ng-focus=\"UserCtrl.resetError()\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-4\">\n" +
    "            <label for=\"user_password\">{{ ::AppCtrl.getStrUp('password') }}</label>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-5\">\n" +
    "        <input id=\"user_password\"\n" +
    "               type=\"password\"\n" +
    "               name=\"password\"\n" +
    "               ng-model=\"credentials.password\"\n" +
    "               ng-focus=\"UserCtrl.resetError()\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-4\">\n" +
    "            <button id=\"user_login_button\">{{ ::AppCtrl.getStrUp('login') }}</button>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-5\">\n" +
    "            <span class=\"error_msg\"\n" +
    "                  ng-if=\"UserCtrl.loginError\">\n" +
    "                {{ AppCtrl.getStrUp('error_login') }}\n" +
    "            </span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <a href=\"\" ng-click=\"UserCtrl.registerUser()\">{{ ::AppCtrl.getStrUp('dont_have_account') }}?</a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</form>");
}]);

angular.module("user/user_logout.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("user/user_logout.tpl.html",
    "<div id=\"logout_block\">\n" +
    "    <div>\n" +
    "        <div>{{ ::AppCtrl.getStrUp('welcome') }}</div>\n" +
    "        <div>{{ ::AppCtrl.user.name }}</div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div>\n" +
    "        <a href=\"\" ng-click=\"AppCtrl.logout()\"\n" +
    "           class=\"btn btn-primary\">{{ ::AppCtrl.getStrUp('logout') }}</a>\n" +
    "    </div>\n" +
    "\n" +
    "    <div>\n" +
    "        <a href=\"{{ $data['uri']['changePasswordForm'] }}\"><?= mb_ucfirst(Lang::get('app.change_password')); ?></a>\n" +
    "    </div>\n" +
    "</div>");
}]);
