angular.module('cvManager.auth', [

])

.factory('AuthFactory', ['$http', '$state', 'Session', 'AUTH_URI',
function($http, $state, Session, AUTH_URI) {
    var authFactory = {};

    authFactory.login = function(credentials) {
        return $http
            .post(AUTH_URI.loginPath, credentials)
            .then(function(response) {
                Session.create(response.data.data);
            });
    };

    authFactory.logout = function() {
        return $http
            .post(AUTH_URI.logoutPath, Session.user)
            .then(
                function(response) {
                    if ( response.data.isSuccess === true ) {
                        Session.destroy();
                        $state.go('home');
                    }
            });
    };

    authFactory.isAuthenticated = function() {
        return !!Session.user.id;
    };

    return authFactory;
}])

.service('Session', [
function() {
    this.user = {};

    //initialize the service if page is reloaded for previously logged in user
    this.init = function() {
        try {
            if ( globalUser.id && globalUser.email ) {
                this.create(globalUser);
            }
        } catch(error) {

        }

    };

    this.create = function(user) {
        this.user.id    = user.id;
        this.user.email = user.email;
        this.user.name  = user.name;
    };

    this.destroy = function() {
        this.user = {};
    };
}])

.factory('cvHttpInterceptor', ['$q', '$window',
function($q, $window) {
    return {
        request: function(config) {


            return config;
        },

        responseError: function(rejection) {
            if ( rejection.status === 401 ) {
                $window.location = '/';
            }

            return $q.reject(rejection);
        }
    };
}])

.constant('AUTH_URI',
    {
        loginPath: '/user/login',
        logoutPath:'/user/logout'
    }
)

.constant('AUTH_EVENTS',
{
    loginSuccess:   'auth-login-success',
    loginFailed:    'auth-login-failed',
    logoutSuccess:  'auth-logout-success',
    sessionTimeOut: 'auth-session-timeout',
    notAuthenticated:'auth-not-authenticated',
    notAuthorized:  'auth-not-authorized'
})

;