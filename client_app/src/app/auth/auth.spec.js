describe('Test auth module:', function() {
    "use strict";
    var $httpBackend, AuthFactory, AUTH_URI, $state, Session, AUTH_EVENTS;

    var id      = 1,
        email   = 'example@example.com',
        name    = 'John';

    beforeEach(module('cvManager.auth'));
    beforeEach(module('cvManager.home'));
    beforeEach(module('ui.router'));

    beforeEach(inject(function(_$httpBackend_, _$state_,
                               _AuthFactory_, _AUTH_URI_, _Session_, _AUTH_EVENTS_) {
        $state      = _$state_;
        AuthFactory = _AuthFactory_;
        $httpBackend = _$httpBackend_;
        AUTH_URI    = _AUTH_URI_;
        Session     = _Session_;
        AUTH_EVENTS = _AUTH_EVENTS_;
    }));

    afterEach (function () {
        $httpBackend.verifyNoOutstandingExpectation ();
        $httpBackend.verifyNoOutstandingRequest ();
    });

    it('AuthFactory.login() should return a user', inject(function() {
        var credentials = {
            username:   'user',
            password:   'password'
        };

        $httpBackend
            .whenPOST(AUTH_URI.loginPath, credentials)
            .respond({data: {
                id:     id,
                email: email,
                name: name
            }});

        var user = AuthFactory.login(credentials);
        $httpBackend.flush();

        expect(Session.user).toBeDefined();
        expect(Session.user.id).toBeDefined(id);
        expect(Session.user.email).toBeDefined(email);
        expect(Session.user.name).toBeDefined(name);
    }));

    it('AuthFactory.logout() should destroy Session.user', inject(function() {
        expect(Session.user.id).toBe(undefined);

        Session.user = {
            id:     id,
            email:  email,
            name:   name
        };

        expect(Session.user.id).toBe(id);
        expect(Session.user.email).toBe(email);
        expect(Session.user.name).toBe(name);

        $httpBackend
            .whenPOST(AUTH_URI.logoutPath)
            .respond({
                isSuccess: true
            });

        spyOn($state, 'go').andReturn(true);

        AuthFactory.logout();
        $httpBackend.flush();

        expect(Session.user.id).toBe(undefined);
        expect(Session.user.email).toBe(undefined);
        expect(Session.user.name).toBe(undefined);
    }));


    it('Check that AUTH_URI has values', inject(function() {
        expect(AUTH_URI.loginPath).toBeDefined();
        expect(AUTH_URI.logoutPath).toBeDefined();
    }));


    it('check that AUTH_EVENT to have values', inject(function() {
        expect(AUTH_EVENTS).toBeDefined();
        expect(Object.keys(AUTH_EVENTS).length).toBe(6);

        expect(AUTH_EVENTS.loginSuccess).toBeDefined();
        expect(AUTH_EVENTS.loginFailed).toBeDefined();
        expect(AUTH_EVENTS.logoutSuccess).toBeDefined();
        expect(AUTH_EVENTS.sessionTimeOut).toBeDefined();
        expect(AUTH_EVENTS.notAuthenticated).toBeDefined();
        expect(AUTH_EVENTS.notAuthorized).toBeDefined();
    }));
});

describe('Test auth module Session:', function() {
    "use strict";
    var Session;

    beforeEach(module('cvManager.auth'));

    beforeEach(inject(function() {

    }));

});