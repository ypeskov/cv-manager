angular.module('cvManager.user', [
    'ui.router',

    'cvManager.bikeLocale',
    'cvManager.auth',
    'cvManager.cv-manager'
])

.config(['$stateProvider', 'USER_REGISTER_CONST', 'bikeLocaleProvider',
function($stateProvider, USER_REGISTER_CONST, bikeLocaleProvider) {
    "use strict";
    var bikeLocale = bikeLocaleProvider.$get();

    $stateProvider
        .state(USER_REGISTER_CONST.stateName, {
            url: '/' + USER_REGISTER_CONST.stateName,

            views: {
                "main": {
                    controller: USER_REGISTER_CONST.ctrlName,
                    templateUrl: USER_REGISTER_CONST.templateName
                }
            },

            resolve: {
                token: ['TokenService', function(TokenService) {
                    return TokenService.get();
                }]
            },

            data: {pageTitle: bikeLocale.getStrUp('register_new_user')}
        });
}])
//----------------------------------------------------------------------------

.controller('UserRegisterCtrl', ['$scope', 'UserFactory', 'token', '$state', 'HOME_CONST',
function($scope, UserFactory, token, $state, HOME_CONST) {
    var _self = this;

    _self.token = token.data;
    _self.errors ={};

    _self.register = function(newUser) {
        if ( $scope.user_register_form.$valid ) {

            UserFactory.registerUser(newUser, _self.token)
                .then(function(response) {
                    if ( response.isSuccess !== true ) {
                        if ( response.data.email ) {
                            _self.errors.emailErrorsShow = true;
                            _self.errors.emailErrors = response.data.email.join(' ');
                        }

                        if ( response.data.password ) {
                            _self.errors.passwordErrorsShow = true;
                            _self.errors.passwordErrors = response.data.password.join(' ');
                        }

                    } else {
                        alert('The user has been created. Please login into account.');
                        $state.go(HOME_CONST.homeState);
                    }
                });
        } else {
            angular.forEach($scope.user_register_form.$error.required, function(value, key) {
                value.$setDirty();
            });
        }
    };

    _self.hideEmailErrors = function() {
        _self.errors.emailErrorsShow = false;
    };

    _self.hidePasswordErrors = function() {
        _self.errors.passwordErrorsShow = false;
    };

    return $scope.UserRegisterCtrl = _self;
}])

//----------------------------------------------------------------------------

.constant('USER_REGISTER_CONST',
{
    stateName:      'register-user',
    ctrlName:       'UserRegisterCtrl',
    templateName:   'user/user-register.tpl.html',
    registerUrl:    '/user'
})

//----------------------------------------------------------------------------

.controller('UserCtrl', ['$scope', '$rootScope', '$state',
                        'AUTH_EVENTS', 'AuthFactory', 'Session', 'DASHBOARD_CONST',
                        'USER_REGISTER_CONST', 'TokenService', 'UserFactory',
function($scope, $rootScope, $state, AUTH_EVENTS, AuthFactory, Session, DASHBOARD_CONST,
        USER_REGISTER_CONST, TokenService, UserFactory) {
    var _self = this;

    $scope.credentials = {
        username:   '',
        password:   '',
        _token:     ''
    };

    TokenService.get()
        .then(function(tokenContainer) {
            $scope.credentials._token = tokenContainer.data;
        });

    _self.loginError = false;

    _self.resetError = function() {
        _self.loginError = false;
    };

    _self.login = function(credentials) {
        AuthFactory.login(credentials)
            .then(
            function(response) {
                if ( AuthFactory.isAuthenticated() ) {
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    $state.go(DASHBOARD_CONST.stateName);
                }

            },
            function(response) {
                _self.loginError = true;
            });
    };

    _self.registerUser = function() {
        $state.go(USER_REGISTER_CONST.stateName);
    };

    return $scope.UserCtrl = _self;
}])

//----------------------------------------------------------------------------


.factory('UserFactory', ['Session', '$http', 'USER_REGISTER_CONST',
function(Session, $http, USER_REGISTER_CONST) {
    return {
        getUser: function() {
            return Session.user;
        },

        registerUser: function(user, token) {
            return $http.post(USER_REGISTER_CONST.registerUrl, {
                user:   user,
                _token: token
            })
                .then(function(response) {
                    return response.data;
                },
                function(response) {
                    alert('Some error occurred while adding a user.');
            });
        }
    };
}])

;