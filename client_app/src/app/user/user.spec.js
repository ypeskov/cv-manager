describe('Testing cvManager.user module:', function() {
    "use strict";

    var UserCtrl, createController, $scope;

    beforeEach(module('cvManager.user'));
    beforeEach(module('cvManager.dashboard'));
    beforeEach(module('cvManager.auth'));
    beforeEach(module('cvManager.token'));

    beforeEach(inject(function(_$controller_, $rootScope) {
        $scope = $rootScope.$new();

        createController = function() {
            return _$controller_('UserCtrl', {
                $scope: $rootScope.$new()
            });
        };
    }));

    it('isLoggedin should be defined and false by defult', inject(function() {
        var ctrl = createController();

        expect(ctrl.isLoggedin).toBeFalsy();
    }));

    it('resetError() should be defined', inject(function() {
        var ctrl = createController();

        expect(ctrl.resetError).toBeDefined();
        expect(typeof ctrl.resetError).toBe('function');
    }));

    it('login() should be defined', inject(function() {
        var ctrl = createController();

        expect(ctrl.login).toBeDefined();
        expect(typeof ctrl.login).toBe('function');
    }));

});

describe('Testing UserFactory: ', function() {
    "use strict";
    var UserFactory, Session;

    var id      = 1,
        email   = 'example@example.com',
        name    = 'John';

    beforeEach(module('cvManager.user'));

    beforeEach(function() {
        Session = function() {
            this.user = {
                id:     id,
                email:  email,
                name:   name
            };
        };

        module(function($provide) {
            $provide.service('Session', Session);
        });

        inject(function(Session, _UserFactory_) {
            UserFactory = _UserFactory_;
        });
    });

    it('UserFactory.getUser() should be defined', inject(function() {
        expect(UserFactory.getUser).toBeDefined();
        expect(typeof UserFactory.getUser).toBe('function');
    }));

    it('User should be taken from Session value', inject(function() {
        var user = UserFactory.getUser();

        expect(user.id).toBe(id);
        expect(user.email).toBe(email);
        expect(user.name).toBe(name);
    }));

    it('User should be undefined if Session is empty', inject(function() {
        spyOn(UserFactory, 'getUser').andReturn({});
        var user = UserFactory.getUser();

        expect(user.id).toBe(undefined);
        expect(user.email).toBe(undefined);
        expect(user.name).toBe(undefined);
    }));
});