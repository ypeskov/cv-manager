angular.module('cvManager.cv-form', [
    'ui.router',

    'cvManager.bikeLocale',
    'cvManager.cv-manager',
    'ui.bootstrap.tpls',
    'ui.bootstrap.datepicker'
])

.config(['$stateProvider', 'bikeLocaleProvider', 'NEW_CV_CONST',
    function($stateProvider, bikeLocaleProvider, NEW_CV_CONST) {
        var bikeLocale = bikeLocaleProvider.$get();

        $stateProvider
            .state(NEW_CV_CONST.stateName, {
                url: '/' + NEW_CV_CONST.stateName + '/:cvId',

                resolve: {
                    locales: ['CvFactory', function(CvFactory) {
                        return CvFactory.readAvailableLocales();
                    }]
                },

                views: {
                    "main": {
                        controller: NEW_CV_CONST.ctrlName,
                        templateUrl: NEW_CV_CONST.templateName
                    }
                },

                data: {
                    pageTitle: bikeLocale.getStrUp('add_new_cv')
                }

            });
    }])

.controller('newCvCtrl', ['$scope', '$state', 'CV_MANAGER_CONST', 'CvFactory', 'locales',
        '$stateParams', '$timeout',
function($scope, $state, CV_MANAGER_CONST, CvFactory, locales, $stateParams, $timeout) {
    var _self = this;

    _self.menuItems = [
        {
            label: 'home',
            action: function() {
                $state.go('dashboard');
            }
        },
        {
            label: 'manage CV',
            action: function() {
                $state.go(CV_MANAGER_CONST.stateName);
            }
        }
    ];

    _self.locales = locales;

    _self.cv = CvFactory.getEmptyCv();

    _self.cvId = null;

    if ( $stateParams.cvId ) {
        CvFactory.getCv($stateParams.cvId)
            .then(function(response) {
                _self.cv = response;
                _self.cvId = $stateParams.cvId;
            });
    }

    _self.addSkill = function() {
        _self.cv.skills.push(CvFactory.getEmptySkill());
    };

    _self.removeSkill = function(index) {
        _self.cv.skills.splice(index, 1);
    };

    _self.addEducation = function() {
        _self.cv.educations.push(CvFactory.getEmptyEducation());
    };

    _self.removeEducation = function(index) {
        _self.cv.educations.splice(index, 1);
    };

    _self.addWork = function() {
        _self.cv.workExperiences.push(CvFactory.getEmptyWork());
    };

    _self.removeWork = function(index) {
        _self.cv.workExperiences.splice(index, 1);
    };

    _self.saveCv = function() {
        if ( $scope.new_cv.$valid ) {

            if ( _self.cvId === null ) {
                CvFactory.saveCv(_self.cv)
                    .then(
                        function(data) {
                            if ( data.isSuccess === true ) {
                                $state.go(CV_MANAGER_CONST.stateName);
                            } else {
                                alert('Some error occurred. Please try again.');
                            }
                        });
            } else {
                CvFactory.updateCv(_self.cv, _self.cvId)
                    .then(
                        function(data) {
                            if ( data.isSuccess === true ) {
                                $state.go(CV_MANAGER_CONST.stateName);
                            } else {
                                alert('Some error occurred. Please try again.');
                            }
                        });
            }

        } else {
            angular.forEach($scope.new_cv.$error.required, function(value, key) {
                value.$setDirty();
            });
        }
    };

    _self.birthDateOpened = false;
    _self.open = function() {
        _self.birthDateOpened = true;
    };

    _self.educationCalendarStates = [
        {
            start:  false,
            finish: false
        }
    ];
    _self.openEducationCal = function(i, order) {
        if ( order === 0 ) {
            _self.cv.educations[i].displayStartDate = true;
        } else {
            _self.cv.educations[i].displayFinishDate = true;
        }

    };

    _self.dateFormat = 'dd-MM-yyyy';


    $scope.dateOptions = {
        showWeeks:  false,
        startingDay: 1
    };

    $scope.$watch('newCvCtrl.cv.personal_info.birth_date', function(newVal, oldVal) {
        console.log(oldVal);
        console.log(newVal);
        console.log(newVal.toString());

    });

    return $scope.newCvCtrl = _self;
}])

.factory("CvFactory", ['$http', '$q', "CV_FACTORY_CONST",
    function($http, $q, CV_FACTORY_CONST) {
        //"use strict";
        var _self = this;
        var cvFactory = {
            availableLocales:   []
        };

        function handleSuccess( response ) {
            return( response.data );
        }

        cvFactory.handleError = function(response) {
            alert("An unknown error occurred.");
        };

        cvFactory.getEmptyCv = function() {
            return {
                personal_info:  cvFactory.getEmptyPersonalInfo(),
                skills:         [cvFactory.getEmptySkill()],
                educations:     [cvFactory.getEmptyEducation()],
                workExperiences:[cvFactory.getEmptyWork()]
            };
        };

        cvFactory.getEmptyPersonalInfo = function() {
            return {
                locale_id:      "",
                cv_name:        "",
                first_name:     "",
                last_name:      "",
                birth_date:     "",
                email:          "",
                skype:          "",
                contact_phone:  ""
            };
        };

        cvFactory.getEmptySkill = function() {
            return {
                group_name:     "",
                group_content:  ""
            };
        };

        cvFactory.getEmptyEducation = function() {
            return {
                educational_inst:   "",
                date_start:         "",
                date_finish:        "",
                specialty:          "",
                displayStartDate:   false,
                displayFinishDate:  false
            };
        };

        cvFactory.getEmptyWork = function() {
            return {
                company_name:   "",
                date_start:     "",
                date_finish:    "",
                position:       "",
                description:    ""
            };
        };

        cvFactory.saveCv = function(cv) {
            return $http.post(CV_FACTORY_CONST.cvUrl, {
                cv: cv
            }).then(handleSuccess, cvFactory.handleError);
        };

        cvFactory.updateCv = function(cv, cvId) {
            return $http.put(CV_FACTORY_CONST.cvUrl + "/" + cvId, {
                cv:     cv,
                cvId:   cvId
            }).then(handleSuccess, cvFactory.handleError);
        };

        cvFactory.readAvailableLocales = function() {
            if ( cvFactory.availableLocales.length > 0) {
                return cvFactory.getLocales();
            }

            return $http.get(CV_FACTORY_CONST.localeUrl)
                .then(
                function(response) {
                    cvFactory.availableLocales = response.data;
                    return response.data;
                },
                cvFactory.handleError);
        };

        cvFactory.getLocales = function() {
            return cvFactory.availableLocales;
        };

        cvFactory.getCvList = function() {
            return $http.get(CV_FACTORY_CONST.cvListUrl)
                .then(
                function(response) {
                    if ( response.data.isSuccess === true ) {
                        return response.data.data;
                    } else {
                        return $q.reject("An error");
                    }
                },
                cvFactory.handleError
            );
        };

        cvFactory.getCv = function(cvId) {
            return $http.get('/cv/' + cvId)
                .then(
                    function(response) {
                        var cv = response.data.data;
                        cv.educations.map(function(item, i, arr) {
                            arr[i].displayStartDate = false;
                            arr[i].displayFinishDate = false;
                        });

                        return cv;
                    },
                    cvFactory.handleError
                );
        };

        cvFactory.deleteCv = function(cv) {
            return $http.delete('/cv/' + cv.id)
                .then(function(response){
                    return response.data;
                });
        };

        return cvFactory;
    }])

.constant('CV_FACTORY_CONST',
{
    cvUrl:          "/cv",
    localeUrl:      "/locale",
    cvListUrl:      '/cv',

    generalError:   "Sorry, unexpected error :("
})

.constant('NEW_CV_CONST', {
    stateName:      'new-cv',
    templateName:   'cv-form/new-cv.tpl.html',
    ctrlName:       'newCvCtrl'
})

;