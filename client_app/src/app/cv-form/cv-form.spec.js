describe('Test cv-form module: ', function() {
    "use strict";
    var createController;

    beforeEach(module('cvManager.cv-form'));

    beforeEach(inject(function(_$controller_, _$rootScope_) {
        createController = function() {
                return _$controller_('newCvCtrl', {
                    $scope: _$rootScope_.$new(),
                    locales: []
                });
        };
    }));

    it('newCvCtrl should be defined', inject(function() {
        var ctrl = createController();

        expect(ctrl).toBeDefined();
        expect(ctrl).toBeTruthy();
    }));

    it('menu items should be defined', inject(function() {
        var ctrl = createController();

        expect(ctrl.menuItems.length).toBe(2);
    }));

    it('check that menu actions are callable', inject(function() {
        var ctrl = createController();

        spyOn(ctrl.menuItems[0], 'action').andReturn(true);
        var callResult = ctrl.menuItems[0].action();

        expect(ctrl.menuItems[0].action).toHaveBeenCalled();
        expect(callResult).toBe(true);

        spyOn(ctrl.menuItems[1], 'action').andReturn(true);
        callResult = ctrl.menuItems[1].action();

        expect(ctrl.menuItems[1].action).toHaveBeenCalled();
        expect(callResult).toBe(true);
    }));
});