angular.module('cvManager.calendar', [

])

.directive('cvManagerCalendar', [ '$timeout',
function($timeout) {
    var months = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July',
        'August', 'September', 'October', 'November', 'December'
    ];

    function getNumberDaysInMonth(year, month) {
        var isLeapYear = ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0);

        return [31, (isLeapYear ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    }

    return {
        restrict: "EA",

        replace: true,

        transclude: true,

        scope: {
            ngModel: "="
        },

        templateUrl: 'calendar/calendar.tpl.html',

        link: function ($scope, elem, $attrs, ctrl, transcludeFn) {
            var inputElementWrapper = angular.element(elem.children()[0]);
            var calendarWrapper = angular.element(elem.children()[1]);

            var transcludedElement = null;

            transcludeFn(function (transcludeEl) {
                inputElementWrapper.append(transcludeEl);
                transcludedElement = transcludeEl;

                console.log(transcludedElement.parent());
            });

            transcludedElement.on('focus', function () {
                calendarWrapper.removeClass('cv-calendar-hidden');
            });

            transcludedElement.on('blur', function () {
                $timeout(function() {
                    calendarWrapper.addClass('cv-calendar-hidden');
                }, 500);
            });

            $scope.setD = function (date) {
                console.log(11111111);
                $scope.ngModel = date.val;

                calendarWrapper.addClass('cv-calendar-hidden');
            };

            var today = new Date();

            $scope.month = months[today.getMonth()];
            $scope.year = today.getFullYear();
            $scope.daysQty = getNumberDaysInMonth($scope.year, today.getMonth());
            $scope.todayDate = today.getDate();

            $scope.monthDates = [];
            var i = null;
            for (i = 0; i < $scope.daysQty; i++) {
                $scope.monthDates.push({
                    val: i + 1,
                    current: $scope.todayDate === i + 1 ? true : false
                });

            }
            $scope.monthDates = $scope.monthDates.slice($scope.todayDate - 1);
        }
    };
}])



;