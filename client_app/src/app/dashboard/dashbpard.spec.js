describe('Testing Dashboard module', function() {
    'use strict';

    var DashboardCtrl, createController, UserFactory, DASHBOARD_CONST;

    beforeEach(module('cvManager.dashboard'));
    beforeEach(module('cvManager.user'));

    beforeEach(inject(function(_$controller_, $rootScope, _DASHBOARD_CONST_) {
        UserFactory = {
            getUser: function() {}
        };

        DASHBOARD_CONST = _DASHBOARD_CONST_;

        createController = function() {
            return _$controller_('DashboardCtrl', {
                $scope: $rootScope.$new(),
                UserFactory: UserFactory,
                DASHBOARD_CONST: DASHBOARD_CONST
            });
        };
    }));

    it('DashboardCtrl to be defined', inject(function() {
        var ctrl = createController();

        expect(ctrl).toBeDefined();
        expect(ctrl).toBeTruthy();
    }));

    it('menuItems should be present and length is 3', inject(function() {
        var ctrl = createController();

        expect(ctrl.menuItems.length).toBe(3);

        var i;
        for(i in ctrl.menuItems) {
            expect(ctrl.menuItems[i].label).toBeDefined();
            expect(typeof ctrl.menuItems[i].action).toBe('function');
        }
    }));

    it('DASHBOARD_CONST should be available and defined', inject(function() {
        var ctrl = createController();

        expect(DASHBOARD_CONST).toBeDefined();
        expect(DASHBOARD_CONST.ctrlName).toBe('DashboardCtrl');
    }));
});