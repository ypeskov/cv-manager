angular.module('cvManager.dashboard', [
    'ui.router',

    'cvManager.bikeLocale',
    'cvManager.auth',
    'cvManager.cv-manager'
])

.config(['$stateProvider', 'DASHBOARD_CONST', 'bikeLocaleProvider',
function($stateProvider, DASHBOARD_CONST, bikeLocaleProvider) {
    "use strict";
    var bikeLocale = bikeLocaleProvider.$get();

    $stateProvider
        .state(DASHBOARD_CONST.stateName, {
            url: '/' + DASHBOARD_CONST.stateName,

            views: {
                "main": {
                    controller: DASHBOARD_CONST.ctrlName,
                    templateUrl: DASHBOARD_CONST.templateName
                }
            },

            data: {pageTitle: bikeLocale.getStrUp('dashboard')}
        });
}])

.controller('DashboardCtrl', ['$scope', '$state', 'UserFactory',
        'AuthFactory', 'CV_MANAGER_CONST', 'DASHBOARD_CONST',
function($scope, $state, UserFactory, AuthFactory, CV_MANAGER_CONST, DASHBOARD_CONST) {
    "use strict";
    var _self = this;

    _self.menuItems = [
        {
            label:      'home',
            action:      function() {
                $state.go(DASHBOARD_CONST.stateName);
            }
        },
        {
            label:      'manage CV',
            action:     function() {
                $state.go(CV_MANAGER_CONST.stateName);
            }
        },
        {
            label:      'logout',
            action:     function() {
                AuthFactory.logout();
            }
        }];

    return $scope.DashboardCtrl = _self;
}])

.constant('DASHBOARD_CONST', {
    stateName:      'dashboard',
    templateName:   'dashboard/dashboard.tpl.html',
    ctrlName:       'DashboardCtrl'
})

;