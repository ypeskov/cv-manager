angular.module('cvManager.bikeLocale', [
    'ui.router'
])

.provider('bikeLocale', [
function() {

    /**
     * Service for providing translation.
     * Translation is loaded with initial HTML page.
     *
     * @constructor
     */
    function LocaleService() {
        var _self = this;

        this.availableLocales = [];

        // text resources are loaded in html from server side.
        // verify that they are available otherwise set them empty (the app will work with id)
        try {
            this.resources = textResources;
        } catch(error) {
            this.resources = {};
        }

        /**
        * Return all available language strings.
         */
        this.getStrings = function () {
            return _self.resources;
        };

        /**
         * Return text resource by id.
         *
         * @param id
         * @returns {*}
         */
        this.get = function(id) {
            if ( _self.resources[id] !== undefined ) {
                return _self.resources[id];
            } else {
                return id.toString();
            }
        };

        this.getStrUp = function(id) {
            var text = _self.get(id);

            return text[0].toUpperCase() + text.slice(1);
        };
    }

    this.$get = function () {
        return new LocaleService();
    };
}])

;