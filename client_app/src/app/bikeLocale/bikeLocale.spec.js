describe('testing bikeLocale main methods', function() {
    "use strict";
    var bikeLocale, textResources;

    beforeEach(module('cvManager.bikeLocale'));

    beforeEach(inject(function(_bikeLocale_) {
        bikeLocale = _bikeLocale_;
    }));

    it('resources property should be available and be type of Object', inject(function() {
        expect(bikeLocale.resources instanceof Object).toBeTruthy();
    }));

    it('getStrings() should return correct resources property', inject(function() {
        bikeLocale.resources = {
            text1: 'text 1',
            text2: 'text 2'
        };

        expect(Object.keys(bikeLocale.getStrings()).length).toBe(2);
        expect(bikeLocale.getStrings().text1).toBe('text 1');
        expect(bikeLocale.getStrings().text2).toBe('text 2');
    }));

    it('get() method should return correct string', inject(function() {
        bikeLocale.resources = {
            text1: 'text 1',
            text2: 'text 2'
        };

        expect(bikeLocale.get('text1')).toBe('text 1');
        expect(bikeLocale.get('text2')).toBe('text 2');
    }));

    it('get() should return key if it is absent in resources', inject(function() {
        expect(bikeLocale.get('text1')).toBe('text1');
        expect(bikeLocale.get('text2')).toBe('text2');
    }));

    it('getStrUp() should return capitalized text', inject(function() {
        bikeLocale.resources = {
            text1: 'text 1',
            text2: 'text 2'
        };

        expect(bikeLocale.getStrUp('text 1')).toBe('Text 1');
        expect(bikeLocale.getStrUp('text1')).toBe('Text 1');
    }));
});