angular.module('cvManager.cv-manager', [
    'ui.router',

    'cvManager.bikeLocale',
    'cvManager.header',
    'cvManager.auth',
    'cvManager.cv-form'
])


.config(['$stateProvider', 'CV_MANAGER_CONST', 'bikeLocaleProvider',
function($stateProvider, CV_MANAGER_CONST, bikeLocaleProvider) {
    "use strict";
    var bikeLocale = bikeLocaleProvider.$get();

    $stateProvider
        .state(CV_MANAGER_CONST.stateName, {
            url: '/' + CV_MANAGER_CONST.stateName,

            views: {
                "main": {
                    controller: CV_MANAGER_CONST.ctrlName,
                    templateUrl: CV_MANAGER_CONST.templateName
                }
            },

            data: {pageTitle: bikeLocale.getStrUp('CV Manager')}
        });
}])

.controller('CvManagerCtrl', ['$scope', '$state', 'AuthFactory',
        'DASHBOARD_CONST', 'NEW_CV_CONST', 'CvFactory', 'CvManagerService', 'bikeLocale',
function($scope, $state, AuthFactory,
         DASHBOARD_CONST, NEW_CV_CONST, CvFactory, CvManagerService, bikeLocale) {
    "use strict";
    var _self = this;

    _self.menuItems = [
        {
            label:      'home',
            action:      function() {
                $state.go(DASHBOARD_CONST.stateName);
            }
        },
        {
            label:      'new CV',
            action:     function() {
                $state.go(NEW_CV_CONST.stateName);
            }
        },
        {
            label:      'logout',
            action:     function() {
                AuthFactory.logout();
            }
        }];

    CvFactory.getCvList()
        .then(
        function(cvList) {
            _self.cvList = CvManagerService.formatCvDescription(cvList);
        },
        CvFactory.handleError
    );

    _self.editCv = function(cv) {
        $state.go(NEW_CV_CONST.stateName, {cvId: cv.id});
    };

    _self.deleteCv = function(cv) {
        if ( confirm(bikeLocale.getStrUp('sure_want_delete_this_cv?')) ) {
            CvFactory.deleteCv(cv)
                .then(function(response) {
                    if ( response.isSuccess === true ) {
                        _self.cvList = _self.cvList.filter(function(item) {
                            return item.id !== cv.id;
                        });
                    } else {
                        CvFactory.handleError();
                    }
                },
                CvFactory.handleError
            );
        }
    };

    _self.viewHtml = function(cv) {

    };

    _self.viewPdf = function(cv) {

    };

    return $scope.CvManagerCtrl = _self;
}])

.service('CvManagerService', function() {
    this.formatCvDescription = function(cvList) {
        return cvList.map(function(item, index, arr) {
            var str = item.cv_name + ", " + item.first_name;

            if ( item.last_name.length > 0 ) {
                str += ", " + item.last_name;
            }

            str += ", " + item.email;

            str += " (" + item.language_long_name + ")";

            return {
                id:         item.id,
                display:    str
            };
        });
    };
})

.constant('CV_MANAGER_CONST',
{
    stateName:      'cv-manager',
    templateName:   'cv-manager/cv-manager.tpl.html',
    ctrlName:       'CvManagerCtrl'
})

;