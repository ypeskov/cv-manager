describe('Test cv-manager module: ', function() {
    "use strict";
    var createController, CV_MANAGER_CONST, $state;

    beforeEach(module('cvManager.cv-manager'));
    beforeEach(module('cvManager.dashboard'));

    beforeEach(inject(function(_$controller_, $rootScope, _AuthFactory_, _$state_,
                               _DASHBOARD_CONST_, _CV_MANAGER_CONST_, NEW_CV_CONST) {
        CV_MANAGER_CONST = _CV_MANAGER_CONST_;
        $state = _$state_;

        expect($state.get(NEW_CV_CONST.stateName)).toBeDefined();
        expect($state.get(NEW_CV_CONST.stateName)).toBeTruthy();
        expect($state.get(NEW_CV_CONST.stateName).url.length).toBeGreaterThan(0);
        expect($state.get(NEW_CV_CONST.stateName).views instanceof Object).toBeTruthy();
        expect($state.get(NEW_CV_CONST.stateName).data instanceof Object).toBeTruthy();

        createController = function() {
            return _$controller_('CvManagerCtrl', {
                $scope: $rootScope.$new(),
                DASHBOARD_CONST: _DASHBOARD_CONST_,
                AuthFactory:    _AuthFactory_
            });
        };


    }));

    it('CvManagerCtrl should be defined', inject(function() {
        var ctrl = createController();

        expect(ctrl).toBeTruthy();
    }));

    it('menuItems should be available', inject(function() {
        var ctrl = createController();
        var items = ['home', 'new CV', 'logout'];

        var i;
        for(i in ctrl.menuItems) {
            expect(items.indexOf(ctrl.menuItems[i].label)).toBeGreaterThan(-1);
            expect(typeof ctrl.menuItems[i].action).toBe('function');
        }
    }));

    it('CV_MANAGER_CONST  should be defined', inject(function() {
        expect(CV_MANAGER_CONST).toBeDefined();
        expect(Object.keys(CV_MANAGER_CONST).length).toBe(3);
    }));

});
