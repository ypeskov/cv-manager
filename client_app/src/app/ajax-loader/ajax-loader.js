angular.module('cvManager.ajax-loader', [

])

.directive('ajaxLoader', ['$http', '$timeout',
function($http, $timeout) {
    return {
        restrict: 'E',

        replace: true,

        scope: {},

        templateUrl: 'ajax-loader/ajax-loader.tpl.html',

        link: function ($scope, elem, $attrs, ctrl, transcludeFn) {

            $scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            $scope.$watch($scope.isLoading, function (v)
            {
                if (v) {
                    elem.css('display', 'block');
                } else {
                    elem.css('display', 'none');
                }
            });
        }
    };
}])

;