describe('AppCtrl', function () {
    describe('isCurrentUrl', function () {
        var $controller, $location, $scope, mockAuthFactory;

        beforeEach(module('cvManager'));
        beforeEach(module('ui.router'));

        beforeEach(inject(function (_$controller_, _$location_, $rootScope, bikeLocale, AuthFactory) {
            $location = _$location_;
            $scope = $rootScope.$new();
            $controller = _$controller_;
            AuthFactory = AuthFactory;

            AppCtrl = $controller('AppCtrl', {
                $location:      $location,
                $scope:         $scope,
                bikeLocale:     bikeLocale,
                AuthFactory:    AuthFactory
            });
        }));

        it('should pass a dummy test', inject(function () {
            expect(AppCtrl).toBeTruthy();
            expect(AppCtrl).toBeDefined();
        }));

        it('AppCtrl has defined properties',
            inject(function() {
                var expectedProperties = [
                    'lang',
                    'getStrUp',
                    'logout',
                    'user'
                ];
                var prop;

                for(prop in AppCtrl) {
                    expect(expectedProperties.indexOf(prop)).toBeGreaterThan(-1);
                }
        }));

        it('AppCtrl properties are of defined type', inject(function() {
            expect(AppCtrl.logout instanceof Function).toBeTruthy();
            expect(AppCtrl.getStrUp instanceof Function).toBeTruthy();
            expect(AppCtrl.lang instanceof Object).toBeTruthy();
        }));

    });
});

