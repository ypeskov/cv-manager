angular.module('cvManager.header', [
    'ui.router',

    'cvManager.bikeLocale'
])

.controller('HeaderCtrl', ['$scope', '$state', 'AuthFactory',
function($scope, $state, AuthFactory) {
    "use strict";
    var _self = this;

    if ( ! AuthFactory.isAuthenticated() ) {
        _self.loggedIn = false;
    } else {
        _self.loggedIn = true;
    }

    return $scope.HeaderCtrl = _self;
}])

;