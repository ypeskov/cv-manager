describe('HeaderCtrl testing properties', function() {
    "use strict";

    var HeaderCtrl, AuthFactory, createController;

    beforeEach(module('cvManager.header'));
    beforeEach(module('cvManager.auth'));

    beforeEach(inject(function(_$controller_, $rootScope) {
        createController = function() {
            return _$controller_('HeaderCtrl', {
                $scope: $rootScope.$new(),
                AuthFactory: AuthFactory
            });
        };
    }));

    it('HeaderCtrl should be initiated', inject(function() {
        AuthFactory = {
            isAuthenticated: function() {
                return true;
            }
        };

        HeaderCtrl = createController();

        expect(HeaderCtrl).toBeTruthy();
    }));

    it("Authentication should be false by default", inject(function() {
        AuthFactory = {
            isAuthenticated: function() {
                return false;
            }
        };

        HeaderCtrl =createController();

        expect(HeaderCtrl.loggedIn).toBeFalsy();
    }));

    it("isLoggedin should be boolean", inject(function() {
        expect(typeof HeaderCtrl.loggedIn).toBe('boolean');
    }));

    it("isLoggedin is true when Authenticated was proceed correct", inject(function() {
        AuthFactory = {
            isAuthenticated: function() {}
        };
        spyOn(AuthFactory, 'isAuthenticated').andReturn(true);

        HeaderCtrl = createController();

        expect(AuthFactory.isAuthenticated).toHaveBeenCalled();
        expect(HeaderCtrl.loggedIn).toBeTruthy();

    }));

});