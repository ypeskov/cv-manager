describe('Covering HomeCtrl', function () {
    var HomeCtrl, $scope;

    beforeEach(module('cvManager.home'));
    beforeEach(module('cvManager.token'));

    beforeEach(inject(function (_$controller_, $rootScope) {
        $scope = $rootScope.$new();
        $controller = _$controller_;

        HomeCtrl = $controller('HomeCtrl', {
            $scope:     $scope
        });
    }));

    it('should have a dummy test', inject(function () {
        expect(HomeCtrl).toBeTruthy();
    }));
});

describe('Covering HomeMainCtrl', function() {
    var HomeMainCtrl, $scope;

    beforeEach(module('cvManager.home'));

    beforeEach(inject(function(_$controller_, $rootScope) {
        $scope = $rootScope.$new();
        $controller = _$controller_;

        HomeMainCtrl = $controller('HomeMainCtrl', {
            $scope: $scope
        });
    }));

    it('HomeMainCtrl should be initiated OK', inject(function() {
        expect(HomeMainCtrl).toBeTruthy();
    }));
});